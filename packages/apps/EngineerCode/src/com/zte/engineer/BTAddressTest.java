
package com.zte.engineer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice; 
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.provider.Settings;
import android.view.KeyEvent;

public class BTAddressTest extends AddressTest {
    /*
     * Define some aliases to make these debugging flags easier to refer to.
     */
    private final static String TAG = "BTAddressTest";

    TextView mBluetoothStatus;
    TextView mBluetoothAddress;
    TextView mBluetoothDevices;

    BluetoothAdapter mBluetooth;
    IntentFilter filter;

    boolean isManualTurnOn = false;

	
    @Override
    protected void onResume() {
        // registerReceiver(screenoff, new IntentFilter(Screenoff));
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
		registerReceiver(mBluetoothReceiver, filter);
        if (mBluetooth.isDiscovering()) {  
            mBluetooth.cancelDiscovery();  
        }
        mBluetooth.startDiscovery(); 
        super.onResume();

        // PhoneWindowManager.setKeyTestState(true);
    }

    @Override
    protected void onPause() {
        // unregisterReceiver(screenoff);
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        super.onPause();

        // PhoneWindowManager.setKeyTestState(false);
    }	
	
    // Receiver the bluetooth status change
    protected BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // Get bluetooth status change action
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    case BluetoothAdapter.STATE_ON:
                        mBluetoothStatus.setText(String.format(getString(R.string.bt_status),
                                getString(R.string.on)));
                        // Get current bluetooth adapter
                        mBluetooth = BluetoothAdapter.getDefaultAdapter();
                        mBluetoothAddress.setText(String.format(getString(R.string.bt_address_is),
                                mBluetooth.getAddress()));
                        mBluetooth.startDiscovery(); 
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        mBluetoothStatus.setText(String.format(getString(R.string.bt_status),
                                getString(R.string.off)));
                        mBluetoothAddress.setText(String.format(getString(R.string.bt_address_is),
                                getString(R.string.unknown)));
                        break;
                }
            } else if (action.equals(BluetoothDevice.ACTION_FOUND)) {  
                //BluetoothDevice device = intent  
                  //      .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);  
               // if (device.getBondState() != BluetoothDevice.BOND_BONDED) {  
                   // mTextView.append(device.getName() + ":"  
                     //       + device.getAddress()+"\n");  
                         Log.d(TAG,"ACTION_FOUND");
                    mBluetoothDevices.setText(R.string.bt_detect_success);
                   ((Button) findViewById(R.id.btnPass)).setEnabled(true);
                      
                //}  
            } else if (action  
                    .equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {  
                 Log.d(TAG,"ACTION_DISCOVERY_FINISHED");
                //setProgressBarIndeterminateVisibility(false);   
            }  
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setContentView(R.layout.singlebuttonview);
        Log.d("feng", "BTAddressTest->onCreate");

        // Get current bluetooth adapter
        mBluetooth = BluetoothAdapter.getDefaultAdapter();
        // Get current bluetooth status
        boolean isEnable = mBluetooth.isEnabled();

        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mBluetoothStatus = (TextView) findViewById(R.id.singlebutton_textview_2);
        mBluetoothAddress = (TextView) findViewById(R.id.singlebutton_textview_3);
        mBluetoothDevices = (TextView) findViewById(R.id.singlebutton_textview_4);

        mTextView.setText(R.string.bt_address);
        ((Button) findViewById(R.id.btnPass)).setEnabled(false);

        if (true == isEnable) {
            mBluetoothStatus.setText(String.format(getString(R.string.bt_status),
                    getString(R.string.on)));
            mBluetoothAddress.setText(String.format(getString(R.string.bt_address_is),
                    mBluetooth.getAddress()));
        } else {
            mBluetooth.enable();
            isManualTurnOn = true;
            mBluetoothStatus.setText(String.format(getString(R.string.bt_status),
                    getString(R.string.off)));
            mBluetoothAddress.setText(R.string.bt_read_address);
        }
        mBluetoothDevices.setText(R.string.bt_detect_fail);

        // Set intent filter
        filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        // register filter receiver

        // button progress
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);

        // M:fxj
        //mButton.setText(R.string.test_bt_search);
        //Intent intent = new Intent("android.settings.BLUETOOTH_SETTINGS");
        //setTargetIntent(intent);
        // M:end
    }

    @Override
    protected void onDestroy() {
        // unregisterReceiver(mBluetoothReceiver);
        super.onDestroy();
    }

    @Override
    public void finishSelf(int result) {
        unregisterReceiver(mBluetoothReceiver);
        if (true == isManualTurnOn) {
            mBluetooth.disable();
        }
        super.finishSelf(result);
    }

    @Override
    public void onClick(View arg0) {
        Log.d("feng", "BTAddressTest->onClick");

        switch (arg0.getId()) {
            case R.id.btnPass:
                finishSelf(RESULT_PASS);
                break;
            case R.id.btnFail:
                finishSelf(RESULT_FAIL);
                break;
            // M:fxj
            /*case BUTTON_ID:
                doJump();
                ((Button) findViewById(R.id.btnPass)).setEnabled(true);
                break;*/
            // M:end
            default:
                finishSelf(RESULT_PASS);
                break;
        }

    }

}
