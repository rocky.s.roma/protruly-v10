
package com.zte.engineer;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author fxj Parent class to add a search button for BTAddressTest and
 *         WifiAddressTest
 */
public class AddressTest extends ZteActivity {

    private final static String TAG = "AddressTest";
    Button mButton;
    private Intent mIntent;
    protected final static int BUTTON_ID = 111;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("feng", "AddressTest->onCreate");
        setContentView(R.layout.singlebuttonview);
        //initView();
    }

    private void initView() {
        RelativeLayout mRelativeLayout = (RelativeLayout) findViewById(R.id.singlebutton_view);
        RelativeLayout.LayoutParams mLayoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        mButton = new Button(this);
        mButton.setId(BUTTON_ID);
        mButton.setOnClickListener(this);
        mRelativeLayout.addView(mButton, mLayoutParams);
    }

    protected void doJump() {
        preparedJump();
        jumpToSetting();
    }

    protected void setTargetIntent(Intent intent) {
        mIntent = intent;
    }

    protected void jumpToSetting() {
        if (null != mIntent) {
            startActivity(mIntent);
        }
    }

    protected void preparedJump() {

    }

}
