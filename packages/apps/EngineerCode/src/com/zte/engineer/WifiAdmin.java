
package com.zte.engineer;

import java.util.List;
import android.util.Log;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;

public class WifiAdmin {
    public static String TAG = "WifiAdmin";

    private WifiManager mWifiManager;

    private WifiInfo mWifiInfo;

    private List<ScanResult> mWifiList;

    private List<WifiConfiguration> mWifiConfigurations;
    WifiLock mWifiLock;

    public WifiAdmin(Context context) {

        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        mWifiInfo = mWifiManager.getConnectionInfo();
    }

    public void openWifi() {
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }
    }

    public void closeWifi() {
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(false);
        }
    }

    public String checkState() {
        if (mWifiManager.getWifiState() == 3)
            return "Opened";
        else if (mWifiManager.getWifiState() == 1)
            return "Closed";
        return "NoKnow";
    }

    public void acquireWifiLock() {
        mWifiLock.acquire();
    }

    public void releaseWifiLock() {

        if (mWifiLock.isHeld()) {
            mWifiLock.acquire();
        }
    }

    public void createWifiLock() {
        mWifiLock = mWifiManager.createWifiLock("test");
    }

    public List<WifiConfiguration> getConfiguration() {
        return mWifiConfigurations;
    }

    public void connetionConfiguration(int index) {
        if (index > mWifiConfigurations.size()) {
            return;
        }

        mWifiManager.enableNetwork(mWifiConfigurations.get(index).networkId, true);
    }

    public WifiConfiguration CreateWifiInfo(String SSID, String Password, int Type) {
        Log.i(TAG, "SSID:" + SSID + ",password:" + Password);
        WifiConfiguration config = new WifiConfiguration();
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        config.SSID = "\"" + SSID + "\"";

        WifiConfiguration tempConfig = this.IsExsits(SSID);

        if (tempConfig != null) {
            mWifiManager.removeNetwork(tempConfig.networkId);
        } else {
            Log.i(TAG, "IsExsits is null.");
        }

        if (Type == 1) // WIFICIPHER_NOPASS
        {
            Log.i(TAG, "Type =1.");
            config.wepKeys[0] = "\"" + "\"";
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        }
        if (Type == 2) // WIFICIPHER_WEP
        {
            Log.i(TAG, "Type =2.");
            config.hiddenSSID = true;
            config.wepKeys[0] = "\"" + Password + "\"";
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        }
        if (Type == 3) // WIFICIPHER_WPA
        {

            Log.i(TAG, "Type =3.");
            config.preSharedKey = "\"" + Password + "\"";

            config.hiddenSSID = true;
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            // config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            config.status = WifiConfiguration.Status.ENABLED;
        }
        return config;
    }

    /*
     * public static final int SECURITY_NONE = 0; public static final int
     * SECURITY_WEP = 1; public static final int SECURITY_PSK = 2; public static
     * final int SECURITY_EAP = 3; public static int
     * getSecurity(WifiConfiguration config) { if
     * (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK)) {
     * return SECURITY_PSK; } if
     * (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP) ||
     * config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X)) {
     * return SECURITY_EAP; } return (config.wepKeys[0] != null) ? SECURITY_WEP
     * : SECURITY_NONE; }
     */

    private WifiConfiguration IsExsits(String SSID) {
        List<WifiConfiguration> mWifiConfigurations = mWifiManager.getConfiguredNetworks();
        for (WifiConfiguration wifiConfigurations : mWifiConfigurations) {
            if (wifiConfigurations.SSID.equals("\"" + SSID + "\"")) {
                return wifiConfigurations;
            }
        }
        return null;
    }

    public void connetionWifiConfiguration(int index, String password) {

        ScanResult scanRet = mWifiList.get(index);
        // if (scanRet.SSID.equalsIgnoreCase("Du"))

        int temp = 0;
        if (scanRet.capabilities.contains("WEP")) {
            temp = 2;
            Log.e("yuanwenchao", "Ener WEP");
        }

        else if (scanRet.capabilities.contains("WPA")) {
            temp = 3;
            Log.e("yuanwenchao", "Ener WPA");
        }

        else {
            temp = 1;
            Log.e("yuanwenchao", "Ener NOPASSWORD");
        }
        Log.e("yuanwenchao", "scanRet.SSID = " + scanRet.SSID + " password = " + password
                + " temp = " + temp);
        int netID = mWifiManager.addNetwork(CreateWifiInfo(scanRet.SSID, password, temp));
        boolean bRet = mWifiManager.enableNetwork(netID, true);

    }

    public void startScan() {
        mWifiManager.startScan();

        mWifiList = mWifiManager.getScanResults();

        mWifiConfigurations = mWifiManager.getConfiguredNetworks();
    }

    public List<ScanResult> getWifiList() {
        return mWifiList;
    }

    public StringBuffer lookUpScan() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mWifiList.size(); i++) {
            sb.append("Index_" + Integer.valueOf(i + 1).toString() + ":");

            sb.append((mWifiList.get(i)).toString()).append("\n");
        }
        return sb;
    }

    public String getMacAddress() {
        return (mWifiInfo == null) ? "NULL" : mWifiInfo.getMacAddress();
    }

    public String getBSSID() {
        return (mWifiInfo == null) ? "NULL" : mWifiInfo.getBSSID();
    }

    public int getIpAddress() {
        return (mWifiInfo == null) ? 0 : mWifiInfo.getIpAddress();
    }

    public int getNetWordId() {
        return (mWifiInfo == null) ? 0 : mWifiInfo.getNetworkId();
    }

    public String getWifiInfo() {
        return (mWifiInfo == null) ? "NULL" : mWifiInfo.toString();
    }

    public void addNetWork(WifiConfiguration configuration) {
        int wcgId = mWifiManager.addNetwork(configuration);
        mWifiManager.enableNetwork(wcgId, true);
    }

    public void disConnectionWifi(int netId) {
        mWifiManager.disableNetwork(netId);
        mWifiManager.disconnect();
    }
}
