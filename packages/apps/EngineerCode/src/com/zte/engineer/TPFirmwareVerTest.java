
package com.zte.engineer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TPFirmwareVerTest extends ZteActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.normal);
        boolean isInErrStat = true;
        BufferedReader reader = null;
        try {
            Process p = null;
            if (Build.VERSION.SDK_INT == 23) { // M
                p = Runtime.getRuntime().exec("cat /sys/class/input/input6/configid");
            } else if (Build.VERSION.SDK_INT == 21 /*Build.VERSION_CODES.LOLLIPOP*/
                    || Build.VERSION.SDK_INT == 22 /*Build.VERSION_CODES.LOLLIPOP_MR1*/) { // L & L1
                p = Runtime.getRuntime().exec("cat /sys/class/input/input2/configid");
            }
            String tpFirmwareVer = null;

            if (p != null) {
                reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                if (reader != null) {
                    // android.util.Log.d("MyTest",reader.readLine());
                    tpFirmwareVer = reader.readLine();
                }
            }

            if (TextUtils.isEmpty(tpFirmwareVer)) {
                // Read new standard node.
                if (Build.VERSION.SDK_INT >= 23) { // M
                    tpFirmwareVer = Util.getDeviceProc(DevInfoActivity.PROC_PATH_TP_FW);
                }
            }
            ((TextView) findViewById(R.id.normal_textview2))
                .setText(getString(R.string.tp_firmware_ver) + tpFirmwareVer);
            isInErrStat = TextUtils.isEmpty(tpFirmwareVer);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);

        ((Button) findViewById(R.id.btnPass)).setEnabled(!isInErrStat);
    }

    @Override
    public void finishSelf(int result) {
        super.finishSelf(result);
    }

}
