
package com.zte.engineer;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.view.KeyEvent;
import android.provider.Settings;

public class VibratorTest extends ZteActivity {
    /*
     * Define some aliases to make these debugging flags easier to refer to.
     */
    private final static String LOGTAG = "VibrateTest";

    // Set Vibrate frequency, vibrate 500ms, stop 500ms
    // long[] mVibFreq = {500,500};
    long[] mVibFreq = {
            10, 1000, 500, 1000
    }; // zhangle add this one line code for movego while delete the above code
    private Vibrator mVibrator;
    private NotificationManager mLed;
    // Notification for LED test
    private Notification mNotification;
    // timer handle event
    private static final int TIMER_EVENT_TICK = 1;
    // Notificatin ID
    private static final int NOTIFY_LED = 0x1010;

    /*
     * The method id for restart vibrate & LED
     */
    private void changeVibratorLedStatus() {
        if (MyApplication.HAS_NOTIFY_LED_LIGHT) {
            // Stop the predecessor LED test
            mLed.cancel(NOTIFY_LED);
            // change LED color
            if (Color.RED == mNotification.ledARGB) {
                mNotification.ledARGB = Color.GREEN;
            } else if (Color.GREEN == mNotification.ledARGB) {
                mNotification.ledARGB = Color.BLUE;
            } else if (Color.BLUE == mNotification.ledARGB) {
				mNotification.ledARGB = Color.RED;
			}
            // Set new LED config
            mLed.notify(NOTIFY_LED, mNotification);
        }

        // start new vibrator
        // mVibrator.vibrate(this.mVibFreq, -1);
    }

    // TIMER_EVENT_TICK handler
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIMER_EVENT_TICK:
                    changeVibratorLedStatus();
                    // send new TIMER_EVENT_TICK message
                    sendEmptyMessageDelayed(TIMER_EVENT_TICK, 1000);
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.singlebuttonview);

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);

        TextView mTextView1 = (TextView) findViewById(R.id.singlebutton_textview_1);
        mTextView1.setText(R.string.led_prompt);

        mTextView.setText(R.string.vibrator);

        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        if (MyApplication.HAS_NOTIFY_LED_LIGHT) {
            mTextView1.setVisibility(View.VISIBLE);

            mLed = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotification = new Notification.Builder(this).setSmallIcon(R.drawable.icon)
                    .getNotification();
            // mNotification = new Notification();

            mNotification.flags |= Notification.FLAG_SHOW_LIGHTS;
            mNotification.ledARGB = Color.RED;
            // mNotification.setSmallIcon(R.drawable.icon);
        } else {
            mTextView1.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        mHandler.removeMessages(TIMER_EVENT_TICK);
        mVibrator.cancel();

        if (MyApplication.HAS_NOTIFY_LED_LIGHT) {
            mLed.cancel(NOTIFY_LED);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        if (MyApplication.HAS_NOTIFY_LED_LIGHT) {
            mLed.notify(NOTIFY_LED, mNotification);
        }
        mVibrator.vibrate(mVibFreq, 2);

        mHandler.sendEmptyMessageDelayed(TIMER_EVENT_TICK, 1000);
    }

    @Override
    public void finishSelf(int result) {
        mHandler.removeMessages(TIMER_EVENT_TICK);
        mVibrator.cancel();
        if (MyApplication.HAS_NOTIFY_LED_LIGHT) {
            mLed.cancel(NOTIFY_LED);
        }
        super.finishSelf(result);
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnPass:
                finishSelf(RESULT_PASS);
                break;
            case R.id.btnFail:
                finishSelf(RESULT_FAIL);
                break;
            default:
                finishSelf(RESULT_PASS);
                break;
        }
    }

}
