/**
 * The JAVA file is for LCD test activity, it's base on android open source.
 * Add By WeiBo 2010-09-26 
 */

package com.zte.engineer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.UserHandle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Button;
import android.os.PowerManager;
import android.content.Context;
import android.view.KeyEvent;
import android.provider.Settings;

/**
 * The Class is for LCD test 2010-09-26
 * 
 * @author WeiBo
 */
public class LcdTestActivity extends ZteActivity {
    /*
     * Define some aliases to make these debugging flags easier to refer to.
     */
    private final static String LOGTAG = "LcdTestActivity";
    private static final int TIMER_EVENT_TICK = 1;
    // define constants and variables
    private LinearLayout mFSLl;
    private LinearLayout mButton;
    private int mBGColor;
    private boolean isInAutoTest = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.lcd_test_view);
        // hide staus bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Util.log(LOGTAG, "EMLcdTest.onCreate: this=" + this);
        isInAutoTest = getIntent().getBooleanExtra(ZteActivity.EXTRA_IS_AUTOTEST, false);

        mFSLl = (LinearLayout) this.findViewById(R.id.fullscreen_linelayout);
        mFSLl.setOnClickListener(this);

        mButton = (LinearLayout) this.findViewById(R.id.fullscreen_linelayout_button);
        mButton.setVisibility(View.INVISIBLE);
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);

        setDefaultKeyMode(DEFAULT_KEYS_DISABLE);

        // set default color
        mBGColor = Color.WHITE;
        mFSLl.setBackgroundColor(mBGColor);

        if (isInAutoTest) {
            mHandler.sendEmptyMessageDelayed(TIMER_EVENT_TICK, 1400);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        setBrightnessToMax();
    }

    @Override
    public void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        setBrightnessToSystem();
    }

    /*
     * @Override public boolean onKeyDown(int keyCode, KeyEvent event) { boolean
     * ret_value = true; textview = (TextView) findViewById(R.id.tv2); lcdtest =
     * (TextView) findViewById(R.id.lcd_test); if (keyCode ==
     * KeyEvent.KEYCODE_BACK) { switch (mBGColor) { case Color.WHITE: mBGColor =
     * Color.RED; break; case Color.RED: mBGColor = Color.GREEN; break; case
     * Color.GREEN: mBGColor = Color.BLUE; break; case Color.BLUE: mBGColor =
     * Color.BLACK; break; case Color.BLACK: setResult(RESULT_OK); finish();
     * break; } mFSLl.setBackgroundColor(mBGColor); return true; } return
     * ret_value; }
     */

    private void changeLCDColor() {

        switch (mBGColor) {
            case Color.WHITE:
                mBGColor = Color.RED;
                break;
            case Color.RED:
                mBGColor = Color.GREEN;
                break;
            case Color.GREEN:
                mBGColor = Color.BLUE;
                break;
            case Color.BLUE:
                mBGColor = Color.BLACK;
                break;
        }

        mFSLl.setBackgroundColor(mBGColor);
        if (mBGColor == Color.BLACK) {
            mButton.setVisibility(View.VISIBLE);
        }

    }

    // TIMER_EVENT_TICK handler
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIMER_EVENT_TICK:

                    if (mBGColor == Color.BLACK) {
                        mHandler.removeMessages(TIMER_EVENT_TICK);
                        mFSLl.setBackgroundColor(Color.WHITE);
                        mButton.setVisibility(View.VISIBLE);
                    } else {
                        changeLCDColor();
                        sendEmptyMessageDelayed(TIMER_EVENT_TICK, 1400);
                    }
                    break;
            }
        }
    };

    /*public void onBackPressed() {
        if (mBGColor == Color.BLACK) {
            finishSelf(RESULT_PASS);
        }

    }*/

    @Override
    public void finishSelf(int result) {
        // mHandler.removeMessages(TIMER_EVENT_TICK);
        super.finishSelf(result);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fullscreen_linelayout:
                if (!isInAutoTest) {
                    changeLCDColor();
                }
                break;
            case R.id.btnPass:
                finishSelf(RESULT_PASS);
                break;
            case R.id.btnFail:
                finishSelf(RESULT_FAIL);
                break;
            default:
                finishSelf(RESULT_PASS);
                break;
        }
    }

    private void setBrightnessToMax() {
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        android.util.Log.d(
                "feng",
                "pm.getMaximumScreenBrightnessSetting() = "
                        + pm.getMaximumScreenBrightnessSetting());
        pm.setBacklightBrightness(pm.getMaximumScreenBrightnessSetting());
    }

    private void setBrightnessToSystem() {
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        int maxBacklight = pm.getMaximumScreenBrightnessSetting();
        int value = Settings.System.getIntForUser(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, maxBacklight, UserHandle.USER_CURRENT);
        android.util.Log.d("lq_brightness", "value = " + value + "; maxBacklight = " + maxBacklight);
        pm.setBacklightBrightness(value);
    }
}
