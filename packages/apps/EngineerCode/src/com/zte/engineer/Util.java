
package com.zte.engineer;

import android.os.Build;
import android.os.StatFs;
import android.util.Log;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Util {

    public static final String TAG = "EngineerCode";
	public static final int RET_SUCCESS = 1;
	public static final int RET_FAILED = 0;
    private static boolean LOG = true;
	public static final int TOLERANCE_20 = 2;
	public static final int TOLERANCE_30 = 3;
    public static final int TOLERANCE_40 = 4;
    /**
     * In release, DEBUG should be set to false; true:Purpose is to debug codes
     * in eclipse, because some test need permission signature(eg.
     * BacklightTest). false:Release or other.
     */
    public static boolean DEBUG = false;

    private Util() {

    }

    public static final void log(String tag, String info) {
        if (LOG) {
            Log.v(TAG, tag + ":" + info);
        }
    }

    public static long getTotalBytes(StatFs fs) {
        long size = 0;
        if (Build.VERSION.SDK_INT >= 18) { // Build.VERSION_CODES.JELLY_BEAN_MR2
            size = fs.getTotalBytes();
        } else {
            size = fs.getBlockCount() * fs.getBlockSize();
        }
        return size;
    }

    public static long getAvailableBytes(StatFs fs) {
        long size = 0;
        if (Build.VERSION.SDK_INT >= 18) { // Build.VERSION_CODES.JELLY_BEAN_MR2
            size = fs.getAvailableBytes();
        } else {
            size = fs.getAvailableBlocks() * fs.getBlockSize();
        }
        return size;
    }

    public static final String getDeviceProc(String procFilePath) {
        StringBuffer content = new StringBuffer();
        BufferedReader localBufferedReader = null;
        try {
            localBufferedReader = new BufferedReader(
                    new FileReader(procFilePath), 4096);
            String line;

            while ((line = localBufferedReader.readLine()) != null) {
                content.append(line);
                Log.i(TAG, "[getDeviceProc] path :" + procFilePath
                        + ",-->>> line = " + line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (localBufferedReader != null) {
                try {
                    localBufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return content.toString();
    }
	
	
	public static String[] runCmdInEmSvr(int index, int paramNum, int... param) {
        ArrayList<String> arrayList = new ArrayList<String>();
        AFMFunctionCallEx functionCall = new AFMFunctionCallEx();
        boolean result = functionCall.startCallFunctionStringReturn(index);
        Log.e("chengrq","result:" + result);
        boolean result1 = functionCall.writeParamNo(paramNum);
        Log.e("chengrq","result1" + result1);
        for (int i : param) {
            functionCall.writeParamInt(i);
        }
        if (result) {
            FunctionReturn r;
            do {
                r = functionCall.getNextResult();
                if (r.mReturnString.isEmpty()) {
                    break;
                }
                arrayList.add(r.mReturnString);
            } while (r.mReturnCode == AFMFunctionCallEx.RESULT_CONTINUE);
            if (r.mReturnCode == AFMFunctionCallEx.RESULT_IO_ERR) {
                //Log.d("@M_" + TAG, "AFMFunctionCallEx: RESULT_IO_ERR");
                arrayList.clear();
                arrayList.add("ERROR");
            }
        } else {
            //Log.d("@M_" + TAG, "AFMFunctionCallEx return false");
            arrayList.clear();
            arrayList.add("ERROR");
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

	/**
	*
	*/
	public static int doGyroscopeCalibration(int tolerance) {
		Log.e("chengrq","tolerance:" + tolerance);
		String[] ret = runCmdInEmSvr(
				AFMFunctionCallEx.FUNCTION_EM_SENSOR_DO_GYROSCOPE_CALIBRATION, 1,
				tolerance);
		if (ret.length > 0 && String.valueOf(RET_SUCCESS).equals(ret[0])) {
			return RET_SUCCESS;
		}
		return RET_FAILED;
	}

	public static int clearGyroscopeCalibration() {
        String[] ret = runCmdInEmSvr(
                AFMFunctionCallEx.FUNCTION_EM_SENSOR_CLEAR_GYROSCOPE_CALIBRATION, 0);
        if (ret.length > 0 && String.valueOf(RET_SUCCESS).equals(ret[0])) {
            return RET_SUCCESS;
        }
        return RET_FAILED;
    }
}
