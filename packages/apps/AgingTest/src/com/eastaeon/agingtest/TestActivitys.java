package com.eastaeon.agingtest;

/**
 * Created by zengtao on 2017/5/8.
 */

public class TestActivitys {
    private String mPackageName;
    private String mClassName;
    private String mTestName;
    private boolean isChecked = true;

    public TestActivitys(String packageName, String className, String testName, boolean isSelect) {
        mClassName = className;
        mPackageName = packageName;
        mTestName = testName;
        isChecked = isSelect;
    }

    public void setPagName (String packageName) {
        mPackageName = packageName;
    }

    public void setClzName (String className) {
        mClassName = className;
    }

    public void setTestName (String testName) {
        mClassName = testName;
    }

    public void setCheckedTest (boolean isTest) {
        isChecked = isTest;
    }

    public String getPagName () {
        return mPackageName;
    }

    public String getClzName () {
        return mClassName;
    }

    public String getTestName () {
        return mTestName;
    }

    public boolean getChecked () {
        return isChecked;
    }
}
