package com.eastaeon.agingtest;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class HornTest extends Activity implements View.OnClickListener {
    private final static String TAG = "HornTest";
    private final static int TEST_FAIL = 99;
    private MediaPlayer mMediaP = null;
    private AudioManager mAudioManager = null;
    private final static int MESSAGE_START_PLAY = 1;
    private final static int MESSAGE_STOP_PLAY = 2;
    Button mStop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_horn_test);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
        mStop = (Button) findViewById(R.id.id_stop_horn);
        mStop.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaP = MediaPlayer.create(HornTest.this, R.raw.backroad);
        mMediaP.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e("HornTest", "Error occurred while playing audio.");
                mp.stop();
                mp.release();
                return true;
            }
        });
        mPlayerHandler.sendEmptyMessage(MESSAGE_START_PLAY);
        handler.postDelayed(runnable, 1800000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPlayerHandler.sendEmptyMessage(MESSAGE_STOP_PLAY);
        mAudioManager.setMode(0);
        handler.removeCallbacks(runnable);
        Log.d("HornTest", "onPause");
    }

    private void InitMediaPlayer() {
        mMediaP = new MediaPlayer();
        int max = mAudioManager.getStreamMaxVolume( AudioManager.STREAM_MUSIC );
        mMediaP.setVolume(max, max);
        try {
            mMediaP.setDataSource(this,
                    RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_RINGTONE));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Handler mPlayerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_START_PLAY:
                    InitMediaPlayer();
                    mAudioManager.setMode(AudioManager.MODE_NORMAL);
                    try {
                        Thread.sleep(500);
                    } catch (java.lang.InterruptedException e) {
                    }

                    mMediaP.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mMediaP.setLooping(true);
                    try {
                        mMediaP.prepare();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mMediaP.start();
                    break;
                case MESSAGE_STOP_PLAY:
                    mMediaP.stop();
                    mMediaP.release();
                    mAudioManager.setMode(0);
                    break;
                default:
                    break;
            }
        };
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_stop_horn:
                setResult(TEST_FAIL);
                finish();
        }
    }

    Handler handler=new Handler();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            setResult(Activity.RESULT_OK);
            handler.postDelayed(this, 2000);
            finish();
        }
    };

    private long mExitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Object mHelperUtils;
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();

            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
