package com.eastaeon.agingtest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import android.os.Build;
import android.Manifest;
import android.content.pm.PackageManager;

public class MainActivity extends Activity {
    private String TAG = "AgingTest";
    TextView mTextView;
    ListView testList;
    List<TestActivitys> mTestActivitys = new ArrayList<TestActivitys>();
    Context mContext;
    String pagNames [] = {"com.eastaeon.agingtest", "com.eastaeon.agingtest","com.eastaeon.agingtest","com.eastaeon.agingtest","com.eastaeon.agingtest", "com.eastaeon.agingtest", "com.eastaeon.agingtest"};
    String clzName [] = {"com.eastaeon.agingtest.VibrateTest", "com.eastaeon.agingtest.ReceiverTest",
            "com.eastaeon.agingtest.HornTest", "com.eastaeon.agingtest.CameraTest", "com.eastaeon.agingtest.FrontCameraTest", "com.eastaeon.agingtest.CameraVRTest", "com.eastaeon.agingtest.FrontCameraVRTest"};
    boolean isChecklist [] = {true, true, true, true, true, true, true};
    List<TestActivitys> mTestItems = new ArrayList<TestActivitys>();
    private MyAdapter myAdapter;
    private SharedPreferences mSp;
    private Button mStart;
    private CheckBox mLoop;
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

	private void checkCameraLaunchPermissions(){
		if(checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
			android.util.Log.d("hzh_aging","requestPermissions");
             requestPermissions(new String[] {Manifest.permission.CAMERA},
                        1);
			return;
        }
		android.util.Log.d("hzh_aging","requestPermissions fail");
		doOnCreate();
	}
	private void doOnCreate(){
		android.util.Log.d("hzh_aging","doOnCreate");
		mSp = getSharedPreferences("aging_test", Activity.MODE_PRIVATE);
        mStart = (Button) findViewById(R.id.id_start);
        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(0);
            }
        });
        mLoop = (CheckBox) findViewById(R.id.loop_select);
        mLoop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = mSp.edit();
                if (mLoop.isChecked()) {
                    Log.e("zengtao", "onClick: mLoop = "+mLoop.isChecked());
                    mLoop.setChecked(true);
                    editor.putBoolean("isLoop", true);
                } else {
                    Log.e("zengtao", "onClick: mLoop 1 = "+mLoop.isChecked());
                    mLoop.setChecked(false);
                    editor.putBoolean("isLoop", false);
                }
                editor.commit();
            }
        });
        Log.e("zengtao", "onClick: mLoop 2 = "+mLoop.isChecked());
        mLoop.setChecked(mSp.getBoolean("isLoop", false));
        mTextView = (TextView) findViewById(R.id.textView);
        for (int i=0; i<isChecklist.length; i++) {
            isChecklist[i] = mSp.getBoolean(String.valueOf(i), true);
        }
        String items [] = getResources().getStringArray(R.array.test_case);
        for (int i=0; i<items.length; i++) {
            mTestActivitys.add(new TestActivitys(pagNames[i], clzName[i], items[i], isChecklist[i]));
        }
        mTestItems.clear();
        for (int i = 0; i<mTestActivitys.size(); i++) {
            if(mTestActivitys.get(i).getChecked()) {
                mTestItems.add(mTestActivitys.get(i));
            }
        }
        if (mTestItems.size() <= 0) {
            mStart.setClickable(false);
        } else {
            mStart.setClickable(true);
        }
        myAdapter = new MyAdapter(this);
        testList = (ListView) findViewById(R.id.test_list);
        testList.setAdapter(myAdapter);
        testList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick: "+mTestActivitys.get(position).getChecked());
                if (mTestActivitys.get(position).getChecked()) {
                    mTestActivitys.get(position).setCheckedTest(false);
                } else {
                    mTestActivitys.get(position).setCheckedTest(true);
                }
            }
        });
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		android.util.Log.d("hzh_aging","onRequestPermissionsResult");
		if(requestCode==1){
			if(permissions[0].equals(Manifest.permission.CAMERA)&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
				doOnCreate();
				return;
			}else{
				finish();
			}
			
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		checkCameraLaunchPermissions();
		//doOnCreate();
        
    }

    private final static int TEST_FAIL = 99;

    private void startActivityForResult(int index) {
        Intent intent = new Intent();
        intent.setClassName(mTestItems.get(index).getPagName(), mTestItems.get(index).getClzName());
        startActivityForResult(intent, index);
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == TEST_FAIL) {
			mTextView.setText(R.string.test_msg);
			Log.d(TAG, "onActivityResult: test fail");
			return;			
		}
		int index = requestCode + 1;
		if (index >= mTestItems.size()) {
			mTextView.setText(R.string.test_msg_success);
			if (mSp.getBoolean("isLoop", false)) {
				Log.e("zengtao", "test can not exit");
				if (mTestItems.size() != 1){
					startActivityForResult(0);
				}
				return;
			} else {
				return;
			}
		}
		if (resultCode == TEST_FAIL) {
			mTextView.setText(R.string.test_msg);
			Log.d(TAG, "onActivityResult: test fail");
		} else if (resultCode == Activity.RESULT_OK) {
			startActivityForResult(index);
		}
	}

    public final class ViewHolder {
        public CheckBox select_box;
        public TextView select_name;
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return mTestActivitys.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.select_box = (CheckBox) convertView.findViewById(R.id.list_select);
                holder.select_name = (TextView) convertView.findViewById(R.id.list_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Log.e("zengtao", "getView: holder.select_name = "+holder.select_name);
            holder.select_name.setText(mTestActivitys.get(position).getTestName());
            holder.select_box.setChecked(mTestActivitys.get(position).getChecked());
            holder.select_box.setOnClickListener(new MyCheckClickListener(position));
            return convertView;
        }

        class MyCheckClickListener implements View.OnClickListener {
            private int position;
            public MyCheckClickListener(int index) {
                position = index;
            }

            @Override
            public void onClick(View v) {
                CheckBox myCheck = (CheckBox) v;
                SharedPreferences.Editor editor = mSp.edit();
                Log.d(TAG, "onClick: myCheck.isChecked() = "+myCheck.isChecked());
                if (myCheck.isChecked()) {
                    mTestActivitys.get(position).setCheckedTest(true);
                    editor.putBoolean(String.valueOf(position), true);
                } else {
                    mTestActivitys.get(position).setCheckedTest(false);
                    editor.putBoolean(String.valueOf(position), false);
                }
                editor.commit();
                mTestItems.clear();
                for (int i=0; i<mTestActivitys.size(); i++) {
                    if (mTestActivitys.get(i).getChecked()) {
                        mTestItems.add(mTestActivitys.get(i));
                    }
                }
                if (mTestItems.size() <= 0) {
                    mStart.setClickable(false);
                } else {
                    mStart.setClickable(true);
                }
                myAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return mTestActivitys.size();
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }
    }

    private long mExitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Object mHelperUtils;
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();

            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
