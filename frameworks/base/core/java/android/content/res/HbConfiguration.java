package android.content.res;

import android.app.ActivityManagerNative;
import android.graphics.Canvas;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

/**
 * configuration for theme .
 * @author alexluo
 *
 */
public class HbConfiguration implements Comparable<HbConfiguration>{

	public static final int CONFIG_THEME_FLAG = 0x80000000;
	
	public int themeChanged;
	
	@Override
	public int compareTo(HbConfiguration target) {
		// TODO Auto-generated method stub
		int n = themeChanged - target.themeChanged;
		if(n != 0){
			return n;
		}
		return 0;
	}

	public void setTo(HbConfiguration config){
		themeChanged = config.themeChanged;
	}
	
	public void setToDefaults(){
		themeChanged = 0;
	}
	
	public int updateFrom(HbConfiguration config){
		int changed= 0;
		if (this.themeChanged < config.themeChanged) {
			changed = config.themeChanged | CONFIG_THEME_FLAG;
			this.themeChanged = config.themeChanged;
		}
		return changed;
	}
	
	
	public void readFromParcel(Parcel parcel){
		themeChanged = parcel.readInt();
	}
	
	public void writeToParcel(Parcel parcel){
		parcel.writeInt(themeChanged);
	}
	
	
	public int diff(HbConfiguration config){
		int diff = 0;
		if (this.themeChanged < config.themeChanged)
			diff = 0x0 | CONFIG_THEME_FLAG;
		return diff;
	}
	
	public void updateTheme(long flag){
		if(themeChanged == Integer.MAX_VALUE){
			themeChanged = 0;
		}
		this.themeChanged += 1;
	}
	
	/**
	 * 调用这个接口更新主题并重新加载界面
	 * @param changedValue
	 */
	public static final void updateThemeConfiguration(int changedValue){
		if (changedValue != 0){
		try {
			Configuration config = ActivityManagerNative
					.getDefault().getConfiguration();
			config.themeConfig.updateTheme(changedValue);
			ActivityManagerNative.getDefault().updateConfiguration(
					config);
			Canvas.freeTextLayoutCaches();
			Canvas.freeCaches();
			return;
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		}
	}

	public static boolean needNewResources(int configChanges) {
		// TODO Auto-generated method stub
		return ((CONFIG_THEME_FLAG & configChanges) != 0);
	}
	
	
}
