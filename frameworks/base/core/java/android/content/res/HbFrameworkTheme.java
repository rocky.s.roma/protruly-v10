package android.content.res;


import android.content.res.HbThemeZip.ThemeFileInfo;
import android.util.Log;

/**
 * hb资源hb-framework-res.apk
 * @author alexluo
 *
 */
public class HbFrameworkTheme extends HbApplicationTheme {

	private static HbFrameworkTheme mInstance;
	
	  // Locker
    private static final Object sLock = new Object();
	
	private HbFrameworkTheme(Resources res, String packageName) {
		super(res, packageName);
		// TODO Auto-generated constructor stub
	}
	
	public static HbFrameworkTheme getInstance(Resources res, String packageName){
				mInstance = new HbFrameworkTheme(res, packageName);
			mInstance.update();
			return mInstance;
	}
	
	@Override
	public Integer getThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeInteger(id);
	}
	@Override
	public Float getFloatThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeFloat(id);
	}
	
	
	
	@Override
	public ThemeFileInfo getThemeFileInfo(String themePah,int id) {
		return mThemeZipFile.getThemeFileInfo(HbApplicationTheme.SYSTEM_RES_HUMMINGBIRD, themePah);
	}
	
	@Override
	public String getVectorPath(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getVectorPath(id);
	}
	

}
