package com.dmyk.android.telephony;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.SystemProperties;
import android.telephony.CellLocation;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

import com.android.ims.ImsManager;

import com.mediatek.custom.CustomProperties;
import com.mediatek.telephony.TelephonyManagerEx;

public final class DmykTelephonyManager extends DmykAbsTelephonyManager {
    private static final int LENGTH_MEID = 14;
    private static final int LENGTH_ICCID = 20;
    private static final String PROPERTY_ICCID_SIM_PREFIX = "ril.iccid.sim";
    private static final String PROPERTY_MULTIPLE_IMS_SUPPORT = "ro.mtk_multiple_ims_support";

    public static final class MLog {
        private static final String TAG = "CTM";
        private static final boolean sEnable = false;
        public static void v(String msg) {
            if (sEnable) {
                android.util.Log.v(TAG, msg);
            }
        }
        public static void d(String msg) {
            if (sEnable) {
                android.util.Log.d(TAG, msg);
            }
        }
        public static void i(String msg) {
            if (sEnable) {
                android.util.Log.i(TAG, msg);
            }
        }
        public static void w(String msg) {
            if (sEnable) {
                android.util.Log.w(TAG, msg);
            }
        }
        public static void e(String msg) {
            if (sEnable) {
                android.util.Log.e(TAG, msg);
            }
        }
        public static void wtf(String msg) {
            if (sEnable) {
                android.util.Log.wtf(TAG, msg);
            }
        }
    }

    private Context mContext;
    private TelephonyManager mTelephonyManager = null;
    //private ConnectivityManager mConnectivityManager = null;

    public DmykTelephonyManager(Context context) {
        MLog.d("DmykTelephonyManager instantiated");
        if (context == null) {
            MLog.e("DmykTelephonyManager: context is null!");
        }
        mContext = context;
    }

    private TelephonyManager getTelephonyManager() {
        if (mTelephonyManager == null) {
            if (mContext != null) {
                mTelephonyManager =
                    (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            }
        }
        return mTelephonyManager;
    }

    @Override
    public int getPhoneCount() {
        // MTK proprietary API
        int result = getTelephonyManager().getSimCount();
        MLog.d("getPhoneCount(): " + result);
        return result;
    }

    @Override
    public String getGsmDeviceId(int phoneId) {
        String result = null;
        if (phoneId >= 0 && phoneId < 2) {
            // the sole parameter of getImei is slotId
            result = getTelephonyManager().getImei(phoneId);
        }
        MLog.d("getGsmDeviceId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public String getCdmaDeviceId() {
        String result = null;
        String ACTION_IMEI_MEID = "intent_action_imei_meid";
        Intent intent = mContext
                .registerReceiver(null, new IntentFilter(ACTION_IMEI_MEID));
        if (intent != null) {
            result = intent.getStringExtra("extra_key_meid");
            if (result != null && result.length() == LENGTH_MEID) {
                MLog.d("getCdmaDeviceId(): " + result);
                return result;
            }
        }
        MLog.d("getCdmaDeviceId(): null");
        return null;
    }

    @Override
    public String getSubscriberId(int phoneId) {
        String result = null;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                result = getTelephonyManager().getSubscriberId(subId);
            }
        }
        MLog.d("getSubscriberId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public String getIccId(int phoneId) {
        String result = null;
        TelephonyManagerEx tme = TelephonyManagerEx.getDefault();
        if (phoneId >= 0 && phoneId < 2) {
            result = tme.getSimSerialNumber(phoneId);
        }
        MLog.d("getIccId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getDataState(int phoneId) {
        int result = DATA_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                // MTK proprietary API
                result = getTelephonyManager().getDataState(subId);
            }
        }
        MLog.d("getDataState(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getSimState(int phoneId) {
        int result = DATA_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            // MTK proprietary API
            // getSimState() requires a SIM slot ID
            result = getTelephonyManager().getSimState(phoneId);
        }
        MLog.d("getSimState(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getNetworkType(int phoneId) {
        int result = NETWORK_TYPE_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                // MTK proprietary API
                // getNetworkType() requires a sub ID
                result = getTelephonyManager().getNetworkType(subId);
                MLog.d("TelephonyManager.getNetworkType(" + phoneId + "): " + result);
                if (result == 0) {
                    result =
                        getTelephonyManager().getVoiceNetworkType(subId);
                        MLog.d("getVoiceNetworkType(" + phoneId + "): " + result);
                }
            }
        }
        MLog.d("getNetworkType(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public String getDeviceSoftwareVersion() {
        String result = CustomProperties.getString(
                            CustomProperties.MODULE_DM,
                            "SoftwareVersion",
                            "MTK");
        MLog.d("getDeviceSoftwareVersion(): " + result);
        return SystemProperties.get("ro.build.display.id");
    }

    /*
     * This method should be left to vendor to customize as there is no promising method to detect
     * device type automatically.
     *
     * @return DEVICE_TYPE_CELLPHONE as default
     */
    @Override
    public int getDeviceType() {
        int result = DEVICE_TYPE_CELLPHONE;
        MLog.d("getDeviceType(): " + result);
        return result;
    }

    @Override
    public int getMasterPhoneId() {
        int result = SystemProperties.getInt("persist.radio.simswitch", -1) - 1;
        if (result != 0 && result != 1) {
            result = -1;
        }
        MLog.d("getMasterPhoneId(): " + result);
        return result;
    }

    @Override
    public boolean isInternationalNetworkRoaming(int phoneId) {
        boolean result = false;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                // MTK proprietary API
                // getNetworkType() requires a sub ID
                result = getTelephonyManager().isNetworkRoaming(subId);
            }
        }
        MLog.d("isInternationalNetworkRoaming(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getVoLTEState(int phoneId) {
        int result = VOLTE_STATE_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            if (!supportsMultiIms()) {
                if (phoneId == 0) {
                    boolean enabled =
                            ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
                    result = enabled ? VOLTE_STATE_ON : VOLTE_STATE_OFF;
                }
            } else {
                // phoneId specified in parameter list is actually slot ID
                phoneId = SubscriptionManager.getPhoneId(getSubIdForSlotId(phoneId));
                try {
                    Method method = ImsManager.class.getDeclaredMethod(
                        "isEnhanced4gLteModeSettingEnabledByUser",
                        Context.class,
                        Integer.class);
                    boolean enabled =
                        (Boolean) method.invoke(null, new Object[]{mContext, phoneId});
                    result = enabled ? VOLTE_STATE_ON : VOLTE_STATE_OFF;
                } catch (NoSuchMethodException e1) {
                    MLog.d("No isEnhanced4gLteModeSettingEnabledByUser(Context, int)");
                    if (phoneId == 0) {
                        boolean enabled =
                            ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
                        result = enabled ? VOLTE_STATE_ON : VOLTE_STATE_OFF;
                    } else {
                        result = VOLTE_STATE_UNKNOWN;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    result = VOLTE_STATE_UNKNOWN;
                }
            }
        }
        MLog.d("getVoLTEState(" + phoneId + "): " + result);
        return result;
    }

    public void setVoLTEState(int phoneId, int status) {
        MLog.d("setVoLTEState(" + phoneId + ", " + status + ")");
        if (phoneId >= 0 && phoneId < 2) {
            if (supportsMultiIms()) {
                // phoneId specified in parameter list is actually slot ID
                phoneId = SubscriptionManager.getPhoneId(getSubIdForSlotId(phoneId));
                try {
                    Method method = ImsManager.class.getDeclaredMethod(
                        "setEnhanced4gLteModeSetting",
                        Context.class,
                        Boolean.class,
                        Integer.class);
                    method.invoke(
                        null,
                        new Object[]{mContext, (status == VOLTE_STATE_ON), phoneId});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (phoneId == 0) {
                    ImsManager.setEnhanced4gLteModeSetting(
                        mContext,
                        (status == VOLTE_STATE_ON));
                }
            }
        } else {
            MLog.e("Invalid phoneId, do nothing");
        }
    }


    /**
     * Get APN content uri for Slot ID. Internally APN content uris are defined
     * by Sub ID, so we have to map Slot ID to Sub ID first.
     * @param phoneId slot ID
     * @return APN uri of the slot specified as phoneId, null if no SIM in slot
     */
    @Override
    public Uri getAPNContentUri(int phoneId) {
        String result = null;
        if (phoneId >= 0 && phoneId < 2) {
            if(!isOP01Sim(phoneId)){
                MLog.d("getContentUri(" + phoneId + "): null");
                return null;
            }
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                result = "content://telephony/carriers/preferapn/subId/" + subId;
            }
        }
        MLog.d("getContentUri(" + phoneId + "): " + result);
        return result == null ? null : Uri.parse(result);
    }

    /**
     * @param phoneId the phoneId here is actually subId
     * @return slot ID
     */
    @Override
    public int getSlotId(int phoneId) {
        return SubscriptionManager.getSlotId(phoneId);
    }

    @Override
    public int getCellId(int phoneId) {
        int result = -1;
        if (getSubIdForSlotId(phoneId) == -1){
            MLog.d("getCellId(" + phoneId + "): " + result);
            return  result;
        }
        if (phoneId >= 0 && phoneId < 2) {
            TelephonyManagerEx tme = TelephonyManagerEx.getDefault();
            if (tme == null) {
                MLog.w("TelephonyManagerEx may not be ready");
            } else {
                // TelephonyManagerEx.getCellLocation(int) accepts slot ID
                CellLocation cl = tme.getCellLocation(phoneId);
                if (cl == null) {
                    MLog.e("CellLocation is null");
                } else if (cl instanceof GsmCellLocation) {
                    result = ((GsmCellLocation)cl).getCid();
                } else if (cl instanceof CdmaCellLocation) {
                    result = ((CdmaCellLocation)cl).getSystemId();
                } else {
                    MLog.e("CellLocation type " + cl.getClass() + " is not supported");
                }
            }
        }
        MLog.d("getCellId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getLac(int phoneId) {
        int result = -1;
        if (getSubIdForSlotId(phoneId) == -1){
            MLog.d("getLac(" + phoneId + "): " + result);
            return  result;
        }
        if (phoneId >= 0 && phoneId < 2) {
            TelephonyManagerEx tme = TelephonyManagerEx.getDefault();
            if (tme == null) {
                MLog.w("TelephonyManagerEx may not be ready");
            } else {
                // TelephonyManagerEx.getCellLocation(int) accepts slot ID
                CellLocation cl = tme.getCellLocation(phoneId);
                if (cl == null) {
                    MLog.e("CellLocation is null");
                } else if (cl instanceof GsmCellLocation) {
                    result = ((GsmCellLocation)cl).getLac();
                } else if (cl instanceof CdmaCellLocation) {
                    result = ((CdmaCellLocation)cl).getNetworkId();
                } else {
                    MLog.e("CellLocation type " + cl.getClass() + " is not supported");
                }
            }
        }
        MLog.d("getLac(" + phoneId + "): " + result);
        return result;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public boolean supportsMultiIms() {
        if (SystemProperties.getInt(PROPERTY_MULTIPLE_IMS_SUPPORT, 1) == 1) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isOP01Sim(int phoneId) {
        String operator = null;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId == -1){
                MLog.d("isOP01Sim (" + phoneId + "),subId is -1, return false");
                return  false;
            }
            operator = TelephonyManager.getDefault().getSimOperator(subId);
            MLog.d("operator (" + phoneId + ") :" + operator);
            if(operator == null || operator.equals("") || operator.length() < 4){
                MLog.d("operator number get fail");
                return false;
            }
            if(operator.equals("46000") || operator.equals("46002") || operator.equals("46004") ||
               operator.equals("46007")){
                MLog.d("isOP01Sim(" + phoneId + "): true");
                return true;
            }
        }
        MLog.d("isOP01Sim(" + phoneId + "): false");
        return false;
    }
    private int getSubIdForSlotId(int slotId) {
        // MTK proprietary API
        if (!getTelephonyManager().hasIccCard(slotId)) {
            MLog.d("getSubIdForSlotId(" + slotId + "): -1 (no sim found)");
            return -1;
        }
        // MTK proprietary API
        int[] subIds = SubscriptionManager.getSubId(slotId);
        if (subIds == null || subIds.length < 1 || subIds[0] < 0) {
            MLog.e("getSubIdForSlotId(" + slotId + "): -1 (error)");
            return -1;
        }
        MLog.d("getSubIdForSlotId(" + slotId + "): " + subIds[0]);
        return subIds[0];
    }
}
