package com.mediatek.rcs.message.plugin;

import android.content.Intent;

import com.mediatek.mms.ipmessage.DefaultIpStatusBarSelectorExt;
import com.mediatek.widget.CustomAccountRemoteViews.AccountInfo;
import java.util.ArrayList;

/**
 * Plugin implements. response StatusBarSelectorCreator.java in MMS host.
 *
 */
public class RcsStatusBarSelector extends DefaultIpStatusBarSelectorExt {

    @Override
    public boolean onIpRefreshData(ArrayList<AccountInfo> data) {
        /**
         * the first one must the always ask, if want to hide it, must set the item without icon (
         * the first param below). So the solution is: remove the origin item of 'always ask' then
         * new a item without icon.
         */
        Intent autoIntent = new Intent("");
        data.remove(0);
        AccountInfo alwaysAsk = new AccountInfo(0, null, "", null, autoIntent, false);
        data.add(0, alwaysAsk);
        return true;
    }
}
