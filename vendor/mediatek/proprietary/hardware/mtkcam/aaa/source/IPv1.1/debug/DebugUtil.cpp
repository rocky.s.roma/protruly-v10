/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein is
* confidential and proprietary to MediaTek Inc. and/or its licensors. Without
* the prior written permission of MediaTek inc. and/or its licensors, any
* reproduction, modification, use or disclosure of MediaTek Software, and
* information contained herein, in whole or in part, shall be strictly
* prohibited.
*
* MediaTek Inc. (C) 2010. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
* ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
* WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
* WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
* NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
* RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
* INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
* TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
* RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
* OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
* SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
* RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
* ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
* RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
* MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
* CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek
* Software") have been modified by MediaTek Inc. All revisions are subject to
* any receiver's applicable license agreements with MediaTek Inc.
*/
#define LOG_TAG "DebugUtil"
#include <debug/DebugUtil.h>
#include <aaa_log.h>
#include <isp_tuning.h>

DebugUtil*
DebugUtil::
getInstance(int i4SensorDevId)
{
    switch (i4SensorDevId)
    {
        case NSIspTuning::ESensorDev_Main:
            return DebugUtilDev<NSIspTuning::ESensorDev_Main>::getInstance();
        case NSIspTuning::ESensorDev_Sub:
            return DebugUtilDev<NSIspTuning::ESensorDev_Sub>::getInstance();
        case NSIspTuning::ESensorDev_MainSecond:
            return DebugUtilDev<NSIspTuning::ESensorDev_MainSecond>::getInstance();
#ifdef MTK_SUB2_IMGSENSOR
        case NSIspTuning::ESensorDev_SubSecond:
            return DebugUtilDev<NSIspTuning::ESensorDev_SubSecond>::getInstance();
#endif
        default:
            MY_LOGW("Unsupport sensor device ID: %d\n", i4SensorDevId);
            return NULL;
    }
}

void
DebugUtil::
update(std::string moduleName, std::string tag, int value)
{
    android::Mutex::Autolock lock(m_Lock);
    std::map<std::string,IDebugEntry>::iterator it;

    it = mMap.find(moduleName);
    if(it == mMap.end()){
        IDebugEntry pEntry;
        pEntry.update(tag,value);
        mMap[moduleName] = pEntry;
    } else
        it->second.update(tag,value);
}

void
DebugUtil::
dump()
{
    android::Mutex::Autolock lock(m_Lock);
    std::map<std::string,IDebugEntry>::iterator it;
    for(it = mMap.begin(); it != mMap.end(); ++it){
        MY_LOGW(" (%d) module : %s +", m_i4SensorDevId, it->first.c_str());
        it->second.dump();
        MY_LOGW(" (%d) module : %s -", m_i4SensorDevId, it->first.c_str());
    }
}

void
DebugUtil::
clear()
{
    android::Mutex::Autolock lock(m_Lock);
    if(!mMap.empty())
        mMap.clear();
}
