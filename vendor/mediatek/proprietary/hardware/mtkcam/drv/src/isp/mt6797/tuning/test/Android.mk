#--SWO delete-- #
#--SWO delete-- # ImageIo_Test
#--SWO delete-- #
#--SWO delete-- LOCAL_PATH := $(call my-dir)
#--SWO delete-- 
#--SWO delete-- include $(CLEAR_VARS)
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- LOCAL_SRC_FILES := \
#--SWO delete--     main.cpp \
#--SWO delete--     main_tuningmgr.cpp \
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- # Note: "/bionic" and "/external/stlport/stlport" is for stlport.
#--SWO delete-- LOCAL_C_INCLUDES +=  \
#--SWO delete--     $(TOP)/bionic \
#--SWO delete--     $(TOP)/external/stlport/stlport \
#--SWO delete-- #
#--SWO delete-- # camera Hardware
#--SWO delete-- LOCAL_C_INCLUDES +=  \
#--SWO delete--     $(TOP)/$(MTK_PATH_CUSTOM)/kernel/imgsensor/inc \
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- # Sensor common include header
#--SWO delete-- LOCAL_C_INCLUDES += $(TOP)/kernel-3.4/drivers/misc/mediatek/imgsensor/inc
#--SWO delete-- 
#--SWO delete-- 
#--SWO delete-- # vector
#--SWO delete-- LOCAL_SHARED_LIBRARIES := \
#--SWO delete--     libcutils \
#--SWO delete--     libutils \
#--SWO delete--     libstlport \
#--SWO delete-- 
#--SWO delete-- # Imem/IspDrv/Isp_drv_p1/isp_drv_p2
#--SWO delete-- LOCAL_SHARED_LIBRARIES +=  libcamdrv_imem
#--SWO delete-- 
#--SWO delete-- LOCAL_SHARED_LIBRARIES +=  libfeatureiodrv
#--SWO delete-- 
#--SWO delete-- LOCAL_SHARED_LIBRARIES +=  libcam_utils
#--SWO delete-- #
#--SWO delete-- LOCAL_STATIC_LIBRARIES := \
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- LOCAL_WHOLE_STATIC_LIBRARIES := \
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- LOCAL_MODULE := swo_alias_libcamdrv_tuning_test
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- LOCAL_MODULE_TAGS := eng
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- LOCAL_PRELINK_MODULE := false
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- # Start of common part ------------------------------------
#--SWO delete-- -include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk
#--SWO delete-- 
#--SWO delete-- #-----------------------------------------------------------
#--SWO delete-- LOCAL_CFLAGS += $(MTKCAM_CFLAGS)
#--SWO delete-- 
#--SWO delete-- #-----------------------------------------------------------
#--SWO delete-- LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/middleware/common/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/gralloc_extra/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/ext/include
#-----------------------------------------------------------
ifeq ($(TARGET_BUILD_VARIANT), user)
MTKCAM_LOGENABLE_DEFAULT   := 0
else
MTKCAM_LOGENABLE_DEFAULT   := 1
endif

#-----------------------------------------------------------
LOCAL_CFLAGS += -DMTKCAM_LOGENABLE_DEFAULT=$(MTKCAM_LOGENABLE_DEFAULT)
#--SWO delete-- 
#--SWO delete-- #-----------------------------------------------------------
#--SWO delete-- LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/include
#--SWO delete-- 
#--SWO delete-- # End of common part ---------------------------------------
#--SWO delete-- #
#--SWO delete-- include $(BUILD_EXECUTABLE)
#--SWO delete-- 
#--SWO delete-- 
#--SWO delete-- #
#--SWO delete-- #include $(call all-makefiles-under,$(LOCAL_PATH))
