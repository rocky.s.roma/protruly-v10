/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>

#if !(defined(WIN32) || defined(_X64))
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#endif

#include "FsPrint.h"
#include "FS_I2C.h"

#ifndef I2C_SLAVE_FORCE
#define I2C_SLAVE_FORCE 0x0706  /* Use this slave address, even if it
                                    is already in use by a driver! */
#endif

#define UNUSED(x) (void)(x)

static void hexdump(int nBytes, const uint8_t* data) {
    int i;
    for(i = 0; i < nBytes; i++) {
        PRINT("0x%02x ", data[i]);
    }
}

int __FsI2CWriteRead(int fd, int nWriteBytes, const uint8_t* writeData,
        int nReadBytes, uint8_t *readData, unsigned int *pError) {
    int dev;
    int ret;
    dev = writeData[0];
    ret = ioctl (fd, I2C_SLAVE_FORCE, dev);

    if ( ret < 0 ) {
        PRINT_ERROR("fs16xx - Failed to open i2c slave:0x%02x\n", dev);
        ret = -errno;
        goto exit;
    }

    if(nWriteBytes > 2) {
        ret = write(fd, writeData + 1, nWriteBytes - 1);
    }

    if(nReadBytes) {
        ret = write(fd, writeData + 1, 1);
    
        if(ret < 0) {
            *pError = FS_I2C_NoAck;
        } else {
            ret = read(fd, readData + 1, nReadBytes - 1);
        }
    }

    if(ret < 0) {
        *pError = FS_I2C_NoAck;
        PRINT_ERROR("fs16xx - i2c io error!");
    } else {
        ret = ret + 1;
        *pError = FS_I2C_OK;
    }

exit:
    return ret;
}

int __FsI2CWrite(int fd, int size, uint8_t *buffer, unsigned int *pError) {
    return __FsI2CWriteRead(fd, size, buffer, 0, NULL, pError);
}

int __FsI2CInit(char *devpath) {
    static int sc_fd = -1;
    static char sc_devpath[FILENAME_MAX];

    if(devpath) {
        if(strlen(devpath) >= FILENAME_MAX) {
            PRINT_ERROR("Invalid device path");
            return -1;
        }
        strcpy(sc_devpath, devpath);
    }

    if(sc_fd != -1) {
        if(close(sc_fd)) {
            PRINT_ERROR("Failed to close I2C device");
            return -1;
        }
    }

    sc_fd= open(sc_devpath, O_RDWR | O_NONBLOCK | O_EXCL, 0);
    if (sc_fd < 0) {
        PRINT_ERROR("fs16xx Failed to open i2c bus:%s\n", sc_devpath);
    }

    return sc_fd;
}

int __FsI2CClose(int  fd ) {

    UNUSED(fd);
    // Nothing to do for Android, i2c dev handle won't be closed.
    return 0;
}

int __FsI2cVersion(char *buffer, int fd) {

    UNUSED(fd);
    UNUSED(buffer);
    PRINT("Foursemi i2c version not implemented! \n");
    return 0;
}

