/*
 * Copyright (C) 2016 Fourier Semiconductor Inc.
 *
 */
#define LOG_NDEBUG 0
#define LOG_TAG "fstool"

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#ifdef __ANDROID__
#include <getopt.h>
#include <ctype.h>
#include <log/log.h>
#endif
#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>
#include <media/mediaplayer.h>
#include <media/MediaPlayerInterface.h>
#include "FsTool.h"
#include "FsPrint.h"
#include "FS_I2C.h"
#include "Fs16xx_Custom.h"
#include "FsSocketServer.h"
#include "Fs16xx_regs.h"

#define STATIC_ASSERT( condition, name )\
    typedef char assert_failed_ ## name [ (condition) ? 1 : -1 ];

#ifndef FSDEVICE_I2C_ADDR_R
const int gFsDeviceStereoMode = 0;
const int gDevI2CAddrLeft = FSDEVICE_I2C_ADDR;
const int gDevI2CAddrRight = 0;

#else

STATIC_ASSERT( FSDEVICE_I2C_ADDR_R != FSDEVICE_I2C_ADDR, "i2c device addresses invalid for stereo mode!");

const int gFsDeviceStereoMode = 1;
const int gDevI2CAddrLeft = FSDEVICE_I2C_ADDR;
const int gDevI2CAddrRight = FSDEVICE_I2C_ADDR_R;
#endif

using namespace std;
using namespace android;

struct option long_options[] = {
    {"forcecalib",no_argument,       NULL, 'c'},
    {"regdump",   optional_argument, NULL, 'd'},
    {"register",  required_argument, NULL, 'r'},
    {"server",    required_argument, NULL, 's'},
    {"playtone",  no_argument,       NULL, 'p'},
    {"help",      no_argument,       0,     1 },
    {0,           0,                 0,     0 },
};

/* This array must parallel long_options[] */
const char *descriptions[] = {
    "force calibration(please make sure clock is on while calibrating)",
    "dump I2C device registers",
    "read register, write if extra arg given",
    "run the socket server with specified port number",
    "play a tone(testing purpose)",
    "print this help screen",
};

uint8_t g_i2cAddr = FSDEVICE_I2C_ADDR;

FsSocketServer *gFsSocketServer = NULL;

void SocketSigHandler(int sig) {
    (void) signal(SIGINT, SIG_DFL);

    if(gFsSocketServer) {
        gFsSocketServer->socketExit();
        delete gFsSocketServer;
        gFsSocketServer = NULL;
    }
}

void SocketExitHandler() {
    if(gFsSocketServer) {
        gFsSocketServer->socketExit();
        delete gFsSocketServer;
        gFsSocketServer = NULL;
    }
}

void print_help() {
    fprintf(stdout,
            "FourierSemi engineering tool.\n");
    fprintf(stdout, "options:\n");
    struct option *opt = long_options;
    const char **desc = descriptions;
    while (opt->name) {
        fprintf(stdout, "\t-%c/--%s%s: %s\n",
                isprint(opt->val) ? opt->val : ' ',
                opt->name,
                (opt->has_arg ? " (argument)" : ""),
                *desc);
        opt++;
        desc++;
    }
}

#define SET_STRING_OPTION(name) do {                                   \
    ASSERT(optarg);                                                    \
    (*name) = strdup(optarg);                                          \
} while(0)

#define SET_INT_OPTION(val) do {                                       \
    ASSERT(optarg);                                                    \
    if (strlen(optarg) >= 2 && optarg[0] == '0' && optarg[1] == 'x') { \
            FAILIF(1 != sscanf(optarg+2, "%x", val),                   \
                   "Expecting a hexadecimal argument!\n");             \
    } else {                                                           \
        FAILIF(1 != sscanf(optarg, "%d", val),                         \
               "Expecting a decimal argument!\n");                     \
    }                                                                  \

int main(int argc, char** argv) {
    int ret = 0;

    int cmd, optArgCount;
    int option_index = 0;
    int portNum;
    unsigned short devAddr = g_i2cAddr;
    int regAddr = 0, regVal = 0;
    unsigned char buffer[FS_I2C_MAX_SIZE];

    while(1) {
        cmd = getopt_long (argc, argv,
                         "cd::r:s:p01",
                         long_options,
                         &option_index);
        /* Detect the end of the options. */
        if (cmd == -1) break;

        switch(cmd) {
            case 1:
                print_help(); _exit(1);
                break;
            case 'r':
                if (optarg && strlen(optarg) >= 2 && optarg[0] == '0' && optarg[1] == 'x') {
                    optArgCount = sscanf(optarg, "0x%02x,0x%04x", &regAddr, &regVal);
                    if(2 == optArgCount) {
                        fprintf(stdout, "W dev:0x%04x reg:0x%04x val:0x%04x", devAddr, regAddr, regVal);
                        ret = fs16xx_write_register16(devAddr, regAddr, regVal);
                        fprintf(stdout, " %s\n", (ret==FS_I2C_OK)?"OK":"Fail");
                    } else {
                        devAddr = g_i2cAddr;
                        if(optarg)
                        fprintf(stdout, "Invalid reg address and value parameter! optarg=%s optArgCount=%d\n", optarg, optArgCount);
                    }
                }
                break;
            case 'd':
                //if (strcmp (long_options[option_index].name, "regdump") == 0) {
                    // Dump all registers
                    if (optarg && strlen(optarg) >= 2 && optarg[0] == '0' && optarg[1] == 'x') {
                        if(1 != sscanf(optarg+2, "%x", &devAddr)) {
                            devAddr = g_i2cAddr;
                        }
                    }
                    ALOGD("register dump on device: 0x%x", (unsigned char)devAddr);
                    fs16xxRegisterDump((unsigned char)devAddr );
                //}
                break;
            case 's':
                // Parsing server port
                if (optarg && strlen(optarg) > 0) {
                    if(1 == sscanf(optarg, "%d", &portNum)) {
                        ALOGD("Run server on port %d", portNum);
                        atexit(SocketExitHandler);
                        (void) signal(SIGINT, SocketSigHandler);
                        gFsSocketServer = new FsSocketServer(portNum);
                        gFsSocketServer->run();
                    }
                }
                break;
            case 'c':
                forcecalibration(optarg);
                break;
            case 'p':
                 playTestTone();
                 break;
            default:
                print_help();
                break;
        }
    }

    return ret;
}

int playTestTone() {
    const char *url = "/system/media/audio/ui/Effect_Tick.ogg";
    android::ProcessState::self()->startThreadPool();

    sp<MediaPlayer> mp = new MediaPlayer();
    int fd = open(url,O_RDONLY);

    if(fd < 0) {
        fprintf(stdout, "Failed to open file: %s\n", url);
        return -1;
    }
    mp->setDataSource(fd,0,0x7fffffffL);
    mp->prepare();
    mp->start();
    while (mp->isPlaying()) {
        Sleep(1);
    }
    mp->disconnect();

    return 0;
}

int fs16xxRegisterDump(unsigned char devAddr) {
    int maxReg = 0xFF;
    unsigned short value;
    int ret;

    for(uint8_t reg = 0; reg < maxReg; reg++) {
        ret = fs16xx_read_register16(devAddr, reg, &value);

        if(FS_I2C_OK == ret) {
            fprintf(stdout, "0x%04x ", value);
        } else {
            fprintf(stdout, "  ---- ");
        }
        if(((reg+1)%0x10) == 0) fprintf(stdout, "\n");
    }
    fprintf(stdout, "\n");
    return 0;
}

int fs16xx_read_register16(unsigned char devAddr, unsigned char regAddr, unsigned short *pValue) {
    int ret;
    const int bytes2write = 1; /* subaddress */
    const int bytes2read = 2;  /* 2 bytes that will contain the data of the register */
    unsigned char dataW[1];
    unsigned char bufferR[2];

    dataW[0] = regAddr;
    bufferR[0] = bufferR[1] = 0;
    ret = FS_I2C_WriteRead(devAddr, bytes2write, dataW, bytes2read, bufferR);

    if(FS_I2C_OK == ret) {
        *pValue = (bufferR[0] << 8) + bufferR[1];
    } else {
        *pValue = 0; // default value
    }

    return ret;
}

int fs16xx_write_register16(unsigned char devAddr, unsigned char regAddr, unsigned short value) {
    int ret;
    unsigned char dataW[FS_I2C_MAX_SIZE];

    dataW[0] = regAddr;
    dataW[1] = (value >> 8) & 0xFF;
    dataW[2] = value & 0xFF;

    ret = FS_I2C_Write(devAddr, 3, dataW);

    return ret;
}

int forcecalibration(char* optarg) {
    int ret;
    unsigned char bufferW[2];

    fs16xx_write_register16(gDevI2CAddrLeft, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY2);
    if(gFsDeviceStereoMode) {
        fs16xx_write_register16(gDevI2CAddrRight, FS16XX_OTPACC_REG, FS16XX_OTP_ACC_KEY2);
    }

    fs16xx_write_register16(gDevI2CAddrLeft, FS16XX_ZMCONFIG_REG, 0x0010);
    fprintf(stdout, "force calibration set for left channel.\n");
    if(gFsDeviceStereoMode) {
        fs16xx_write_register16(gDevI2CAddrRight, FS16XX_ZMCONFIG_REG, 0x0010);
        fprintf(stdout, "force calibration set for right channel.\n");
    }

    fprintf(stdout, "Calibration will be operated on next time speaker on.\n");
    return 0;
}

