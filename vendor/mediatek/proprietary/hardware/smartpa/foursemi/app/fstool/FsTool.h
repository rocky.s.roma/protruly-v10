/*
 * Copyright (C) 2016 Fourier Semiconductor Inc.
 *
 */
#ifndef FSTOOL_H
#define FSTOOL_H

#ifdef __cplusplus
extern "C" {
#endif

int fs16xxRegisterDump(unsigned char devAddr);
int fs16xx_read_register16(unsigned char devAddr, unsigned char regAddr, unsigned short *pValue);
int fs16xx_write_register16(unsigned char devAddr, unsigned char regAddr, unsigned short value);
int forcecalibration(char* optarg);
int playTestTone();

#ifdef __cplusplus
}
#endif

#endif
