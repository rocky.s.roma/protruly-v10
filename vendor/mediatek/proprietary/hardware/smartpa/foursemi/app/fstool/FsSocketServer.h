/*
 * Copyright (C) 2016 Fourier Semiconductor Inc.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#if !(defined(WIN32) || defined(_X64))
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>

#define Sleep(ms) usleep(1000*(ms))
#endif


#define MAXSOCKET_BACKLOG 3

class FsSocketServer {
	enum FS_CMD_TYPE{
		REG_READ =  'REGR',
		REG_WRITE = 'REGW',
	};

public:
    FsSocketServer(int port): m_bRuning(false),
        m_nPort(port),
        m_nSocketListen(-1),
        m_nSocketActive(-1),
        m_bTracing(true) { }

    void run() {
        uint8_t buf[256];
        int count;
        
        if(initSocketListen()) {
            while(1) {
                if(socketAccept() == false) break;

                while(m_nSocketActive != -1) {
                    count = read(m_nSocketActive, buf, sizeof(buf));
                    if(count > 0) {
                        processCmd(m_nSocketActive, buf, count);
                    } else {
                        if(m_bTracing) printf("Client %s disconnected\n", m_strClient);
                        close(m_nSocketActive);
                        m_nSocketActive = -1;
                        Sleep(1);

                        //m_bRuning = false;
                        //initSocketListen();
                    }
                }
            }
        }
    }

    bool processCmd(int sock, const uint8_t *pBuf, int len) {
        char sendBuf[256];
		FS_CMD_TYPE *pType;
		
		pType = (FS_CMD_TYPE*)pBuf;
        if(m_bTracing) {
            printf("recieved: ");
            for(int i=0; i<len; i++) {
                printf("0x%02x ", pBuf[i]);
            }
            printf("type=0x%04X \n", *pType);
		}
		
		switch(*pType) {
			case REG_READ:
			// Reg read
				break;
			case REG_WRITE:
			// Reg write
				break;
			default:
				printf("type=0x%04X \n", *pType);
				break;
		}
		write(sock, sendBuf, strlen(sendBuf));
			
		if(m_bTracing) {
            printf("reply(%d): %s\n", strlen(sendBuf), sendBuf);
        }
        return true;
    }

    void socketExit() {
        struct linger l;
        l.l_onoff  = 1;
        l.l_linger = 0;
        m_bRuning = false;

        fprintf(stdout, "Socket exit.");

        if(m_nSocketListen > 0) {
            shutdown(m_nSocketListen, SHUT_RDWR);
            m_nSocketListen = -1;
        }

        if(m_nSocketActive > 0) {
            setsockopt(m_nSocketActive, SOL_SOCKET, SO_LINGER, &l, sizeof(l));
            shutdown(m_nSocketActive, SHUT_RDWR);
            close(m_nSocketActive);
            m_nSocketActive = -1;
        }
    }

    void setTraceEnable(bool bEnable) {
        m_bTracing = bEnable;
    }

protected:
    bool initSocketListen() {

        if(m_bRuning) {
            fprintf (stderr, "Error, server is already running!\n");
            return false;
        }
        
        if(m_nPort == 0) {
            fprintf (stderr, "Invalid port number.\n");
            return false;
        }

        if(-1 == gethostname(m_strHost,sizeof(m_strHost))) {
            fprintf (stderr, "Error, gethostname.\n");
            return false;
        }

        //atexit(SocketExitHandler);
        //(void) signal(SIGINT, SocketSigHandler);

        memset(&m_serverAddr, 0, sizeof(m_serverAddr));
        m_serverAddr.sin_family = AF_INET;
        m_serverAddr.sin_port = htons(m_nPort);

        m_serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

        fprintf(stdout, "Listening to %s:%d\n", m_strHost, m_nPort);

        m_nSocketListen = socket(AF_INET, SOCK_STREAM, 0);
        if(m_nSocketListen == -1){
            fprintf(stderr, "Failed to create listen socket\n");
            return false;
        }

        if(bind(m_nSocketListen, (struct sockaddr*) &m_serverAddr, sizeof(m_serverAddr)) == -1){
            fprintf(stderr, "Bind error\n");
            return false;
        }

        if(listen(m_nSocketListen, MAXSOCKET_BACKLOG) == -1){
            fprintf(stderr, "Listen error\n");
            return false;
        }

        return m_bRuning = true;
    }

    bool socketAccept() {
        if(!m_bRuning) {
            fprintf(stderr, "Error, socket server is not initialized");
            return false;
        }

        int clientAddrLen = sizeof(m_clientAddr);
        m_nSocketActive = accept(m_nSocketListen, (struct sockaddr*) (&m_clientAddr), (socklen_t*)&clientAddrLen);

        inet_ntop(AF_INET, &m_clientAddr.sin_addr.s_addr, m_strClient, sizeof(m_strClient));
        if(m_bTracing) printf("Received connection from client %s\n", m_strClient);

        //close(m_nSocketListen);
        return true;
    }

private:
    bool m_bRuning;
    int  m_nPort;
    int  m_nSocketListen;
    int  m_nSocketActive;

    char m_strHost[64];
    char m_strClient[INET6_ADDRSTRLEN];
    struct sockaddr_in m_serverAddr;
    struct sockaddr_in m_clientAddr;

    bool m_bTracing;
};

