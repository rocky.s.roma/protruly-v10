/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#ifndef FS16XX_H
#define FS16XX_H

#ifdef __cplusplus
extern "C" {
#endif

/* Type containing all the possible errors that can occur
 *
 */
enum Fs16xx_Error {
    Fs16xx_Error_OK = 0,
    Fs16xx_Error_Bad_Parameter,
    Fs16xx_Error_NotOpen, 
    Fs16xx_Error_OutOfHandles,
    Fs16xx_Error_StateTimedOut,
    Fs16xx_Error_StateInvalid,

    Fs16xx_Error_Not_Implemented,
    Fs16xx_Error_Not_Supported,
    Fs16xx_Error_I2C_Fatal,
    Fs16xx_Error_I2C_NonFatal,
    Fs16xx_Error_Other = 1000,
    Fs16xx_Error_Not_FsDev = 1001,
    Fs16xx_Error_Calib_Error = 1002,
    Fs16xx_Error_Invalid_Preset = 1003
};
typedef enum Fs16xx_Error Fs16xx_Error_t;

enum Fs16xx_Channel {
    Fs16xx_Channel_L,   // I2S left channel
    Fs16xx_Channel_R,   // I2S right channel
    Fs16xx_Channel_L_R, // I2S (left + right channel)/2
    Fs16xx_Channel_Stereo
};
typedef enum Fs16xx_Channel Fs16xx_Channel_t;

enum Fs16xx_Mode {
    Fs16xx_Mode_Normal = 0,
    Fs16xx_Mode_RCV
};
typedef enum Fs16xx_Mode Fs16xx_Mode_t;

enum Fs16xx_PwrState {
    Fs16xx_PwrOn = 0,
    Fs16xx_PwrOff = 1
};
typedef enum Fs16xx_PwrState Fs16xx_PwrState_t;

enum Fs16xx_OutputSel{
    Fs16xx_OutputSel_None = 0,
    Fs16xx_OutputSel_DataL, // I2S left channel output
    Fs16xx_OutputSel_DataR, // I2S right channel output
    Fs16xx_OutputSel_TS // Test tone
};
typedef enum Fs16xx_OutputSel Fs16xx_OutputSel_t;

#define FS_DISABLE  0
#define FS_ENABLE   1

#define FS_UMUTE  0
#define FS_MUTE   1

#define MAX_DEV_HANDLES 4

struct Fs16xx_device_handle {
    int in_use;
    unsigned char device_address;
    unsigned char device_id;
    unsigned char revision;
};

typedef int Fs16xx_devId_t;

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))
struct fs16xx_regs {
	int reg;
	int value;
};

#define UNUSED(x) (void)(x)

#if defined(WIN32) || defined(_X64)
#define __func__ __FUNCTION__
#define snprintf _snprintf
#else
#define Sleep(ms) usleep(1000*(ms))
#endif

/**
 * Open an instance to a given Fs16xx device.
 */
Fs16xx_Error_t fs16xx_open(unsigned char i2cDevAddr,
                 Fs16xx_devId_t *pDevId);

/**
 * Return Fs16xx_Error_OK if device is opened, otherwise return Fs16xx_Error_NotOpen.
 */
Fs16xx_Error_t fs16xx_handle_is_open(Fs16xx_devId_t id);

/**
 * Read an arbitrary I2C register.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @param *pValue
 * @return 16 bit value that was stored in the selected I2C register
 */
Fs16xx_Error_t fs16xx_read_register16(Fs16xx_devId_t id,
                       unsigned char subaddress,
                       unsigned short *pValue);

/**
 * Write an arbitrary I2C register of the Fs16XX.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @param value: 16 bit value to be stored in the selected I2C register
 */
Fs16xx_Error_t fs16xx_write_register16(Fs16xx_devId_t id,
                    unsigned char subaddress,
                    unsigned short value);

/**
 * Read num_bytes of registers values from specified address.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @num_bytes  number of bytes to read
 * @param *pValue, point to bytes values from register
 */
Fs16xx_Error_t fs16xx_bulkread_register(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, unsigned char *pValue);

/**
 * Write num_bytes of registers values to specified address.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @num_bytes  number of bytes to read
 * @param *pValue, point to bytes values to write
 */
Fs16xx_Error_t fs16xx_bulkwrite_register(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, const unsigned char *pValue);

/**
 * Reset all I2C registers
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_init(Fs16xx_devId_t id);

/**
 * Deinitialize fs16xx device
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_deinit(Fs16xx_devId_t id);

/**
 * Close the instance with given id.
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_close(Fs16xx_devId_t id);


/**
 * Set Fs16xx power down or power up state according to the param.
 * @param id to opened instance
 * @param powerdown must be 1 or 0,
 *   1: Fs16xx power down.
 *   0: Fs16xx power on.
 */
Fs16xx_Error_t fs16xx_powerdown(Fs16xx_devId_t id, int powerdown);

/**
 * Select the signal to be put on the left channel of the I2S output.
 * Default value:
 * @param id to opened instance
 * @param output_sel: channel selection, refer to Fs16xx_OutputSel in the header file
 */
Fs16xx_Error_t fs16xx_selectI2S_output_left(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel);

/**
 * Select the signal to be put on the right channel of the I2S output.
 * Default value:
 * @param id to opened instance
 * @param output_sel: channel selection, refer to Fs16xx_OutputSel in the header file
 */
Fs16xx_Error_t fs16xx_selectI2S_output_right(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel);

/**
 * Set the volume of fs16xx
 * Default value:
 * @param id to opened instance
 * @param vLevel volume in level. must be between 0 and 255
 */
Fs16xx_Error_t fs16xx_set_volume_level(Fs16xx_devId_t id,
                  unsigned short vLevel);

/**
 * Set sampleRate
 * Default value:
 * @param id to opened instance
 * @param Samplerate in Hz.  must be 32000, 44100 or 48000
 */
Fs16xx_Error_t fs16xx_set_samplerate(Fs16xx_devId_t id, int sampleRate);

/**
 * Get sampleRate
 * @param id to opened instance
 * @param pSampleRate, pointer to sample rate in Hz.
 */
Fs16xx_Error_t fs16xx_get_samplerate(Fs16xx_devId_t id, int *pSampleRate);


/**
 * Select the I2S input channel.
 * Default value:
 * @param id to opened instance
 * @param channel, channel selection: refer to Fs16xx_Channel_t definition
 */
Fs16xx_Error_t fs16xx_select_channel(Fs16xx_devId_t id, Fs16xx_Channel_t channel);


/**
 * Select the device mode.
 * Default mode: normal mode
 * @param id to opened instance
 * @param mode, mode selection: refer to Fs16xx_Mode_t definition
 */
Fs16xx_Error_t fs16xx_select_mode(Fs16xx_devId_t id, Fs16xx_Mode_t mode);

/**
 * Get the device mode: normal mode or RCV mode
 * @param id to opened instance
 * @param mode, point to mode value to return: refer to Fs16xx_Mode_t definition
 */
Fs16xx_Error_t fs16xx_get_mode(Fs16xx_devId_t id, Fs16xx_Mode_t *pMode);

/**
 * Enable the AEC output.
 */
Fs16xx_Error_t fs16xx_enable_aec_output(Fs16xx_devId_t id);

/**
 * Disable AEC output.
 */
Fs16xx_Error_t fs16xx_disable_aec_output(Fs16xx_devId_t id);

/**
 * Set mute state on/off
 * Default mode: mute off
 * @param id to opened instance
 * @param mute state:
 *  0   unmute
 *  1   mute
 */
Fs16xx_Error_t fs16xx_set_mute(Fs16xx_devId_t id, int mute);

/**
 * Get mute state on/off
 * @param id to opened instance
 * @param pMute, point to mute state
 *  0   unmute
 *  1   mute
 */
Fs16xx_Error_t fs16xx_get_mute(Fs16xx_devId_t id, int *pMute);

/**
 * Return Fs16xx revision id.
 * @param id to opened instance
 */
unsigned short fs16xx_get_device_revision(Fs16xx_devId_t id);

/**
 * Fs16xx calibration procedure. Return OK on success, otherwise
 * return failure.
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_re0_calibration(Fs16xx_devId_t id, int force, int storeResult);

/**
 * Fs16xx f0 calibration procedure. Return OK on success, otherwise
 * return failure.
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_f0_calibration(Fs16xx_devId_t id);

/**
 * Fs16xx enable osc clock.
 * @param id to opened instance
 * @param enable, 0 disable, other values enabled
 */
Fs16xx_Error_t fs16xx_osc_enable(Fs16xx_devId_t id, int enable);

/**
 * Fs16xx enable/disable pll.
 * @param id to opened instance
 * @param enable, 0 set disable, other values set enable
 */
Fs16xx_Error_t fs16xx_pll_enable(Fs16xx_devId_t id, int enable);

/**
 * Fs16xx select pll reference.
 * @param id to opened instance
 * @param enable, 0 set disable, other values set enable
 */
Fs16xx_Error_t fs16xx_pll_ref_sel(Fs16xx_devId_t id, int sel);

Fs16xx_Error_t fs16xx_wait_clk_stable(Fs16xx_devId_t id);

Fs16xx_Error_t fs16xx_wait_boost_ssend(Fs16xx_devId_t id);

#ifdef __cplusplus
}
#endif

#endif
