/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "FsPrint.h"
#include "Fs16xx_regs.h"
#include "Fs16xx_reg_def.h"
#include "FS_I2C.h"
#include "Fs16xx.h"
#include "Fs16xxCalibration.h"
#include "Fs16xxPreset.h"

#include <assert.h>
#define _ASSERT(e)  assert(e)

#if ( defined(WIN32) || defined(_X64) )
#define _ASSERT assert
#endif

static struct Fs16xx_device_handle gDevHandles[MAX_DEV_HANDLES] = {
{
    .in_use = 0,
    .device_address = 0,
    .device_id = 0,
    .revision = 0,
},
{
    .in_use = 0,
    .device_address = 0,
    .device_id = 0,
    .revision = 0,
},
{
    .in_use = 0,
    .device_address = 0,
    .device_id = 0,
    .revision = 0,
},
{
    .in_use = 0,
    .device_address = 0,
    .device_id = 0,
    .revision = 0,
},
};

static unsigned long Fs16xx_last_start_tick_ms = 0;

// configure pll bclk for 48K
static const struct fs16xx_regs Fs16xx_samplerate_48K[] = {
{
    .reg = 0x04,
    .value = 0x880B, // sample rate to 48KHz
},
{
    .reg = 0xC1,
    .value = 0x0260, 
},
{
    .reg = 0xC2,
    .value = 0x0540,
},
{
    .reg = 0xC3,
    .value = 0x000C,
}
};

// configure pll bclk for 44.1K
static const struct fs16xx_regs Fs16xx_samplerate_44_1K[] = {
{
    .reg = 0x04,
    .value = 0x780B, // sample rate to 44.1KHz
},
{
    .reg = 0xC1,
    .value = 0x0260,
},
{
    .reg = 0xC2,
    .value = 0x0460,
},
{
    .reg = 0xC3,
    .value = 0x000A,
}
};

// configure pll bclk for 16K(Voice call)
static const struct fs16xx_regs Fs16xx_samplerate_16K[] = {
{
    .reg = 0x04,
    .value = 0x3818, // sample rate to 16KHz
},
{
    .reg = 0xC1,
    .value = 0x0260,
},
{
    .reg = 0xC2,
    .value = 0x0540 ,
},
{
    .reg = 0xC3,
    .value = 0x0004 ,
}
};

/**
 * Open an instance to a given Fs16xx device.
 */
Fs16xx_Error_t fs16xx_open(unsigned char i2cDevAddr,
                 Fs16xx_devId_t *pDevId) {
    Fs16xx_Error_t err = Fs16xx_Error_OutOfHandles;
    unsigned short rev;
    int i, retry = 50;
    
    _ASSERT(pDevId != (Fs16xx_devId_t *) 0);
    *pDevId = -1;

    for(i = 0; i < MAX_DEV_HANDLES; i++) {
        if(1 == gDevHandles[i].in_use 
            && gDevHandles[i].device_address == i2cDevAddr) {
            PRINT_ERROR("Already opened %d", i2cDevAddr);
            *pDevId = i;
            return Fs16xx_Error_OK;
        }
    }

    for(i = 0; i < MAX_DEV_HANDLES; i++) {
        if(0 == gDevHandles[i].in_use) {
            gDevHandles[i].in_use = 1;
            gDevHandles[i].device_address = i2cDevAddr;

            while(retry > 0) {
                err = fs16xx_read_register16(i, FS16XX_ID_REG, &rev);
                if(Fs16xx_Error_OK == err) break;
                retry--;
                Sleep(2);
            }
            if(Fs16xx_Error_OK != err) {
                gDevHandles[i].in_use = 0;
                break;
            }
            gDevHandles[i].device_id = (rev>>8) & 0xFF;
            gDevHandles[i].revision = rev & 0xFF;

            if(gDevHandles[i].device_id != 0x01
                || gDevHandles[i].revision == 0) {
                PRINT_ERROR("device_id=%d revision=%d", 
                    gDevHandles[i].device_id, gDevHandles[i].revision);
                gDevHandles[i].in_use = 0;
                return Fs16xx_Error_Not_FsDev;
            }
            *pDevId = i;
            err = Fs16xx_Error_OK;
            break;
        }
    }

    return err;
}

/**
 * Return Fs16xx_Error_OK if device is opened, otherwise return Fs16xx_Error_NotOpen.
 */
Fs16xx_Error_t fs16xx_handle_is_open(Fs16xx_devId_t id) {
    Fs16xx_Error_t ret = Fs16xx_Error_OK;
    if(id >= 0 && id < MAX_DEV_HANDLES) {
        if(gDevHandles[id].in_use != 1) {
            ret = Fs16xx_Error_NotOpen;
        }
    }
    return ret;
}

/* translate a I2C driver error into an error for Fs16xxAPI */
static Fs16xx_Error_t fs16xx_translate_i2c_error(FS_I2C_Error_t i2c_error)
{
    switch (i2c_error) {
        case FS_I2C_OK:
            return Fs16xx_Error_OK;
        case FS_I2C_NoAck:
        case FS_I2C_TimeOut:
            return Fs16xx_Error_I2C_NonFatal;
        default:
            return Fs16xx_Error_I2C_Fatal;
    }
}

/**
 * Read an arbitrary I2C register.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @param *pValue
 * @return 16 bit value that was stored in the selected I2C register
 */
Fs16xx_Error_t fs16xx_read_register16(Fs16xx_devId_t id,
                       unsigned char subaddress,
                       unsigned short *pValue) {
    const int bytes2write = 1; /* subaddress */
    const int bytes2read = 2;  /* 2 bytes that will contain the data of the register */
    unsigned char dataW[1];
    unsigned char bufferR[2];
    FS_I2C_Error_t ret;

    _ASSERT(pValue != (unsigned short *)0);
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;
    dataW[0] = subaddress;
    bufferR[0] = bufferR[1] = 0;
    ret = FS_I2C_WriteRead(gDevHandles[id].device_address, bytes2write, dataW, bytes2read, bufferR);

    if(FS_I2C_OK == ret) {
        *pValue = (bufferR[0]<<8) + bufferR[1];
        return Fs16xx_Error_OK;
    } else {
        //*pValue = 0; // default value
        return fs16xx_translate_i2c_error(ret);
    }
}

/**
 * Write an arbitrary I2C register of the Fs16XX.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @param value: 16 bit value to be stored in the selected I2C register
 */
Fs16xx_Error_t fs16xx_write_register16(Fs16xx_devId_t id,
                    unsigned char subaddress,
                    unsigned short value) {
    enum FS_I2C_Error ret;
    int bytes2write = 3;    /* subaddress and 2 bytes of the value */
    unsigned char dataW[3];

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;
    
    dataW[0] = subaddress;
    dataW[1] = (value>>8) & 0xFF;
    dataW[2] = value & 0xFF;
    ret = FS_I2C_Write(gDevHandles[id].device_address, bytes2write, dataW);

#if 0
    // Register write dump
    DEBUGPRINT("reg w=%02X%04X", subaddress, value);
#endif

    if(FS_I2C_OK == ret) {
        return Fs16xx_Error_OK;
    } else {
        return fs16xx_translate_i2c_error(ret);
    }
}

/**
 * Read num_bytes of registers values from specified address.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @num_bytes  number of bytes to read
 * @param *pValue, point to bytes values from register
 */
Fs16xx_Error_t fs16xx_bulkread_register(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, unsigned char *pValue) {
    const int bytes2write = 1;
    unsigned char dataW[1];
    FS_I2C_Error_t ret;
	dataW[0] = subaddress;
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    if(0 == pValue) {
        return Fs16xx_Error_Bad_Parameter;
    }

    ret = FS_I2C_WriteRead(gDevHandles[id].device_address, bytes2write,
                    dataW, num_bytes, pValue);
    if(FS_I2C_OK == ret) {
        return Fs16xx_Error_OK;
    } else {
        return fs16xx_translate_i2c_error(ret);
    }
    
}

/**
 * Write num_bytes of registers values to specified address.
 * @param id to opened instance
 * @param subaddress: 8 bit subaddress of the I2C register
 * @num_bytes  number of bytes to read
 * @param *pValue, point to bytes values to write
 */
Fs16xx_Error_t fs16xx_bulkwrite_register(Fs16xx_devId_t id,
                    unsigned char subaddress, int num_bytes, const unsigned char *pValue) {
    unsigned char dataW[FS_I2C_MAX_SIZE];
    int bytes2write = num_bytes + 1;
    FS_I2C_Error_t ret;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    if(0 == pValue) {
        return Fs16xx_Error_Bad_Parameter;
    }

    dataW[0] = subaddress;
    memcpy(dataW + 1, pValue, num_bytes);
    ret = FS_I2C_Write(gDevHandles[id].device_address, bytes2write, dataW);
    if(FS_I2C_OK == ret) {
        return Fs16xx_Error_OK;
    } else {
        return fs16xx_translate_i2c_error(ret);
    }
}


/**
 * Reset all I2C registers
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_init(Fs16xx_devId_t id) {
    Fs16xx_Error_t err;
    unsigned short val;
    unsigned int i;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;
    
    /* reset all i2C registers to default */
    err = fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, FS16XX_SYSCTRL_REG_I2CR_MSK);
    Sleep(5);
    err |= fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, FS16XX_SYSCTRL_REG_PWDN_MSK);

    err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, 0x000E);

    for (i = 0; i < ARRAY_SIZE(Fs16xx_reg_defaults); i++) {
		err |= fs16xx_write_register16(id, Fs16xx_reg_defaults[i].reg,
			            Fs16xx_reg_defaults[i].value);
    }

    return err;
}


Fs16xx_Error_t fs16xx_deinit(Fs16xx_devId_t id) {
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id)) {
        gDevHandles[id].in_use = 0;
    }
    return Fs16xx_Error_OK;
}

/**
 * Close the instance with given id.
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_close(Fs16xx_devId_t id) {
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id)) {
        gDevHandles[id].in_use = 0;
        return Fs16xx_Error_OK;
    } else {
        return Fs16xx_Error_NotOpen;
    }
}


/**
 * Set Fs16xx power down or power up state according to the param.
 * @param id to opened instance
 * @param powerdown must be 1 or 0,
 *   1: Fs16xx power down.
 *   0: Fs16xx power on.
 */
Fs16xx_Error_t fs16xx_powerdown(Fs16xx_devId_t id, int powerdown) {
    Fs16xx_Error_t err;
    unsigned short valPwr, valBst, valPll, valStatus;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read power state */
    err = fs16xx_read_register16(id, FS16XX_SYSCTRL_REG, &valPwr);
    err |= fs16xx_read_register16(id, FS16XX_BSTCTRL_REG, &valBst);
    err |= fs16xx_read_register16(id, FS16XX_PLLCTRL4_REG, &valPll);

    if(Fs16xx_Error_OK != err) {
        return err;
    }

    switch((Fs16xx_PwrState_t)powerdown) {
        case Fs16xx_PwrOn:
            valPwr &= (~FS16XX_SYSCTRL_REG_PWDN_MSK);
            valBst = (valBst | (FS16XX_BSTCTRL_REG_BSTEN_MSK));
            valPll = 0x000F;

            // Idac gain 0
            err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, (valBst & (~FS16XX_BSTCTRL_REG_DAC_GAIN_MSK)));

            /* Write updated power state */
            err = fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, valPwr);
            err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, valPll); /* Always write default value */
            fs16xx_wait_clk_stable(id);		
            err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, valBst);
            fs16xx_wait_boost_ssend(id);
            break;
        case Fs16xx_PwrOff:
            valPwr |= FS16XX_SYSCTRL_REG_PWDN_MSK;
            valBst &= (~(FS16XX_BSTCTRL_REG_BSTEN_MSK | FS16XX_BSTCTRL_REG_DAC_GAIN_MSK));
            valPll = 0;

            err = fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, valBst);
            err |= fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, valPll);
            err |= fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, valPwr);
            break;
        default:
            return Fs16xx_Error_Bad_Parameter;
    }
    return err;
}

/**
 * Select the signal to be put on the left channel of the I2S output.
 * Default value:
 * @param id to opened instance
 * @param output_sel: channel selection, refer to Fs16xx_OutputSel in the header file
 */
Fs16xx_Error_t fs16xx_selectI2S_output_left(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel) {
    Fs16xx_Error_t err;
    unsigned short value;

    UNUSED(output_sel);

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read I2SSEL register */
    err = fs16xx_read_register16(id, FS16XX_I2SSET_REG, &value);
    
    return Fs16xx_Error_Not_Implemented;
}

/**
 * Select the signal to be put on the right channel of the I2S output.
 * Default value:
 * @param id to opened instance
 * @param output_sel: channel selection, refer to Fs16xx_OutputSel in the header file
 */
Fs16xx_Error_t fs16xx_selectI2S_output_right(Fs16xx_devId_t id,
                        Fs16xx_OutputSel_t output_sel) {
    Fs16xx_Error_t err;
    unsigned short value;

    UNUSED(output_sel);

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read I2SSEL register */
    err = fs16xx_read_register16(id, FS16XX_I2SSET_REG, &value);

    return Fs16xx_Error_Not_Implemented;
}

/**
 * Set the volume of fs16xx
 * Default value:
 * @param id to opened instance
 * @param vLevel volume in level. must be between 0 and 255
 */
Fs16xx_Error_t fs16xx_set_volume_level(Fs16xx_devId_t id,
                  unsigned short vLevel) {
    Fs16xx_Error_t err;
    unsigned short value;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    if(vLevel > 255) {
        vLevel = 255;
    }

    /* Read AUDIOCTRL register */
    err = fs16xx_read_register16(id, FS16XX_AUDIOCTRL_REG, &value);
    if(Fs16xx_Error_OK == err) {
        /* Write volume to AUDIOCTRL register, volume is in bit 15:8 */
        value = (value & 0x00FF) | (unsigned short)(vLevel<<8);
        err = fs16xx_write_register16(id, FS16XX_AUDIOCTRL_REG, value);
    }
    return err;
}

/**
 * Set sampleRate
 * Default value:
 * @param id to opened instance
 * @param Samplerate in Hz.  must be 32000, 44100 or 48000
 */
Fs16xx_Error_t fs16xx_set_samplerate(Fs16xx_devId_t id, int sampleRate) {
    Fs16xx_Error_t err;
    unsigned short value;
    unsigned int i;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Disalbe PLL */
    err = fs16xx_read_register16(id, FS16XX_PLLCTRL4_REG, &value);
    if (Fs16xx_Error_OK == err) {
        err = fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, 
            value & (~FS16XX_PLLCTRL4_REG_PLLEN_MSK));
    }

    if(sampleRate == 48000) {
        for (i = 0; i < ARRAY_SIZE(Fs16xx_samplerate_48K); i++) {
    		fs16xx_write_register16(id, Fs16xx_samplerate_48K[i].reg,
    			            Fs16xx_samplerate_48K[i].value);
        }
    } else if(sampleRate == 44100) {
        for (i = 0; i < ARRAY_SIZE(Fs16xx_samplerate_44_1K); i++) {
    		fs16xx_write_register16(id, Fs16xx_samplerate_44_1K[i].reg,
    			            Fs16xx_samplerate_44_1K[i].value);
        }
    } if(sampleRate == 16000) {
        for (i = 0; i < ARRAY_SIZE(Fs16xx_samplerate_16K); i++) {
    		fs16xx_write_register16(id, Fs16xx_samplerate_16K[i].reg,
    			            Fs16xx_samplerate_16K[i].value);
        }
    } else {
        err = Fs16xx_Error_Not_Implemented;
    }

    
    /* Write back PLL enable */
    fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, value);

    return err;
}

/**
 * Get sampleRate
 * @param id to opened instance
 * @param pSampleRate, pointer to sample rate in Hz.
 */
Fs16xx_Error_t fs16xx_get_samplerate(Fs16xx_devId_t id, int *pSampleRate) {
    Fs16xx_Error_t err;
    unsigned short value;

    if(0 == pSampleRate) {
        return Fs16xx_Error_Bad_Parameter;
    }
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read AUDIOCTRL register */
    err = fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &value);

    if(Fs16xx_Error_OK == err) {
        value &= FS16XX_I2SCTRL_REG_I2SSR_MSK;
        switch(value) {
            case FS16XX_I2SCTRL_RATE_08000:
                *pSampleRate = 8000 ;
                break;
            case FS16XX_I2SCTRL_RATE_16000:
                *pSampleRate = 8000 ;
                break;
            case FS16XX_I2SCTRL_RATE_44100:
                *pSampleRate = 44100;
                break;
            case FS16XX_I2SCTRL_RATE_48000:
            default: // 48000 by default
                *pSampleRate = 48000;
        }
    }

    return err;
}


/**
 * Select the I2S input channel.
 * Default value:
 * @param id to opened instance
 * @param channel, channel selection: refer to Fs16xx_Channel_t definition
 */
Fs16xx_Error_t fs16xx_select_channel(Fs16xx_devId_t id, Fs16xx_Channel_t channel) {
    Fs16xx_Error_t err;
    unsigned short value;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read I2SCTRL register, update CHS12 bits */
    err = fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &value);
    if(Fs16xx_Error_OK == err) {
        /* Clear the 4 bits */
        value &= ~(0x3 << FS16XX_I2SCTRL_REG_CHS12_POS);
        switch(channel) {
            case Fs16xx_Channel_L:
                value |= (0x1<<FS16XX_I2SCTRL_REG_CHS12_POS);
                break;
            case Fs16xx_Channel_R:
                value |= (0x2<<FS16XX_I2SCTRL_REG_CHS12_POS);
                break;
            case Fs16xx_Channel_L_R:
                value |= (0x3<<FS16XX_I2SCTRL_REG_CHS12_POS);
                break;
            case Fs16xx_Channel_Stereo:
                err = Fs16xx_Error_Not_Supported;
                break;
            default:
                err = Fs16xx_Error_Bad_Parameter;
                break;
        }

        if(Fs16xx_Error_OK == err) {
            err = fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value);
        }
    }

    return err;
}


/**
 * Select the device mode.
 * Default mode: normal mode
 * @param id to opened instance
 * @param mode, mode selection: refer to Fs16xx_Mode_t definition
 */
Fs16xx_Error_t fs16xx_select_mode(Fs16xx_devId_t id, Fs16xx_Mode_t mode) {
    Fs16xx_Error_t err;
    unsigned short bstCtrl_val;
    unsigned short dspCtrl_val;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read BSTCTRL register, update MODE_CTRL bits */
    err = fs16xx_read_register16(id, FS16XX_BSTCTRL_REG, &bstCtrl_val);

    /* Read DSPCTRL register, update DSPEN bits */
    err = fs16xx_read_register16(id, FS16XX_DSPCTRL_REG, &dspCtrl_val);

    if(Fs16xx_Error_OK == err) {
        switch(mode) {
            case Fs16xx_Mode_RCV:
                // TODO: Need verify this operation
                /* RCV mode:  set follow mode and bypass DSP */
                bstCtrl_val &= (~FS16XX_BSTCTRL_REG_BSTEN_MSK);
                dspCtrl_val &= (~FS16XX_DSPCTRL_REG_DSPEN_MSK);
                err = fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, bstCtrl_val);
                err = fs16xx_write_register16(id, FS16XX_DSPCTRL_REG, dspCtrl_val);
                break;
            case Fs16xx_Mode_Normal:
                /* Normal mode:  set normal mode and enable DSP */
                bstCtrl_val |= FS16XX_BSTCTRL_REG_BSTEN_MSK;
                dspCtrl_val |= FS16XX_DSPCTRL_REG_DSPEN_MSK;
                err = fs16xx_write_register16(id, FS16XX_DSPCTRL_REG, dspCtrl_val);
                err = fs16xx_write_register16(id, FS16XX_BSTCTRL_REG, bstCtrl_val);
                break;
            default:
                err = Fs16xx_Error_Bad_Parameter;
                break;
        }
    }
    return err;
}

/**
 * Get the device mode: normal mode or RCV mode
 * @param id to opened instance
 * @param mode, point to mode value to return: refer to Fs16xx_Mode_t definition
 */
Fs16xx_Error_t fs16xx_get_mode(Fs16xx_devId_t id, Fs16xx_Mode_t *pMode) {
    Fs16xx_Error_t err;
    unsigned short bstCtrl_val;
    unsigned short dspCtrl_val;

    if(0 == pMode) {
        return Fs16xx_Error_Bad_Parameter;
    }

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read BSTCTRL register */
    err = fs16xx_read_register16(id, FS16XX_BSTCTRL_REG, &bstCtrl_val);

    /* Read DSPCTRL register */
    err = fs16xx_read_register16(id, FS16XX_DSPCTRL_REG, &dspCtrl_val);

    if (Fs16xx_Error_OK == err) {
        if(bstCtrl_val & (FS16XX_BSTCTRL_REG_BSTEN_MSK)) {
            // BST enabled
            //if(dspCtrl_val & (~FS16XX_DSPCTRL_REG_DSPEN_MSK)) {
            //    
            //}
            *pMode = Fs16xx_Mode_Normal;
        } else {
            if((dspCtrl_val & FS16XX_DSPCTRL_REG_DSPEN_MSK) == 0) {
                *pMode = Fs16xx_Mode_RCV;
            } else {
                // Invalid state if bst disabled and dsp enabled
                //*pMode = Fs16xx_Mode_RCV;
                err = Fs16xx_Error_StateInvalid;
            }
        }
    }
    return err;
}

/**
 * Enable the AEC output.
 */
Fs16xx_Error_t fs16xx_enable_aec_output(Fs16xx_devId_t id) {
    Fs16xx_Error_t err;
    unsigned short value;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read I2SCTRL register, update MODE_CTRL bits */
    err = fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &value);

    if (Fs16xx_Error_OK == err) {
        value |= FS16XX_I2SCTRL_REG_I2SDOE;

        err = fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value);
    }
    return err;
}

/**
 * Disable AEC output.
 */
Fs16xx_Error_t fs16xx_disable_aec_output(Fs16xx_devId_t id) {
    Fs16xx_Error_t err;
    unsigned short value;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /* Read I2SCTRL register, update MODE_CTRL bits */
    err = fs16xx_read_register16(id, FS16XX_I2SCTRL_REG, &value);

    if (Fs16xx_Error_OK == err) {
        value &= (~FS16XX_I2SCTRL_REG_I2SDOE);

        err = fs16xx_write_register16(id, FS16XX_I2SCTRL_REG, value);
    }
    return err;
}

/**
 * Set mute state on/off
 * Default mode: mute off
 * @param id to opened instance
 * @param mute state:
 *  0   unmute
 *  1   mute
 */
Fs16xx_Error_t fs16xx_set_mute(Fs16xx_devId_t id, int mute) {
    Fs16xx_Error_t err;
    unsigned short valPwr, valBst, valIdacG, val;
    struct timeval time;
    unsigned long curTickMs, delayMs;

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    /*err = fs16xx_read_register16(id, FS16XX_STATUS_REG, &valPwr);
    if((FS16XX_STATUS_OK_MASK & valPwr) != FS16XX_STATUS_OK_MASK) {
        PRINT_ERROR("%s invalid status = 0x%04X", __func__, valPwr);
        return Fs16xx_Error_StateInvalid;
    }*/

    err = fs16xx_read_register16(id, FS16XX_SYSCTRL_REG, &valPwr);
    err |= fs16xx_read_register16(id, FS16XX_BSTCTRL_REG, &valBst);

    if (Fs16xx_Error_OK == err) {
        switch(mute) {
            case FS_UMUTE:
                err |= fs16xx_write_register16(id, FS16XX_DACCTRL_REG, 0x0200);
                valPwr |= FS16XX_SYSCTRL_REG_AMPE;
                gettimeofday(&time, 0);
                Fs16xx_last_start_tick_ms = 1000 * time.tv_sec + time.tv_usec / 1000;
                err |= fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, valPwr);
                Sleep(10);
                err |= fs16xx_write_register16(id, FS16XX_BSTCTRL_REG,
                    (valBst | FS16XX_BSTCTRL_REG_DAC_GAIN_MSK));

                break;
            case FS_MUTE:
                // Dec-pop begin

                gettimeofday(&time, 0);
                curTickMs = 1000 * time.tv_sec + time.tv_usec / 1000;
                if(curTickMs > Fs16xx_last_start_tick_ms) {
                    delayMs = 21 - (curTickMs - Fs16xx_last_start_tick_ms - 3) % 21;
                    Sleep(delayMs);
                    PRINT_ERROR("%s delay = %ld ms", __func__, delayMs);
                }
                // Dec-pop end

                valPwr &= (~FS16XX_SYSCTRL_REG_AMPE);
                err |= fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, valPwr);
                break;
            default:
                return Fs16xx_Error_Bad_Parameter;
        }
    }
    return err;
}

/**
 * Get mute state on/off
 * @param id to opened instance
 * @param pMute, point to mute state
 *  0   unmute
 *  1   mute
 */
Fs16xx_Error_t fs16xx_get_mute(Fs16xx_devId_t id, int *pMute) {
    Fs16xx_Error_t err;
    unsigned short value;

    if(0 == pMute) {
        return Fs16xx_Error_Bad_Parameter;
    }
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return Fs16xx_Error_NotOpen;

    err = fs16xx_read_register16(id, FS16XX_SYSCTRL_REG, &value);
    if (Fs16xx_Error_OK == err) {
        *pMute = ((value & FS16XX_SYSCTRL_REG_AMPE) == 0) ? 1 : 0;
    }
    return err;
}

/**
 * Return Fs16xx revision id.
 * @param id to opened instance
 */
unsigned short fs16xx_get_device_revision(Fs16xx_devId_t id) {
    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id))
        return 0;

    return gDevHandles[id].revision;
}

/**
 * Fs16xx calibration procedure. Return OK on success, otherwise
 * return failure.
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_re0_calibration(Fs16xx_devId_t id, int force, int storeResult) {

    return fs16xx_calibration(id, force, storeResult);
}

/**
 * Fs16xx f0 calibration procedure. Return OK on success, otherwise
 * return failure.
 * @param id to opened instance
 */
Fs16xx_Error_t fs16xx_f0_calibration(Fs16xx_devId_t id) {

    UNUSED(id);
    return Fs16xx_Error_Not_Implemented;
}

/**
 * Fs16xx enable osc clock.
 * @param id to opened instance
 * @param enable, 0 disable, other values enabled
 */
Fs16xx_Error_t fs16xx_osc_enable(Fs16xx_devId_t id, int enable) {
    Fs16xx_Error_t err;
    unsigned short value;

    /* Read PLLCTRL4 register, update OSCEN bit */
    err = fs16xx_read_register16(id, FS16XX_PLLCTRL4_REG, &value);
    if (Fs16xx_Error_OK == err) {
        if(enable != 0) {
            value |= FS16XX_PLLCTRL4_REG_OSCEN_MSK;
        } else {
            value &= (~FS16XX_PLLCTRL4_REG_OSCEN_MSK);
        }
        if(Fs16xx_Error_OK == err) {
            err = fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, value);
        }
    }
    return err;
}

/**
 * Fs16xx enable/disable pll.
 * @param id to opened instance
 * @param enable, 0 set disable, other values set enable
 */
Fs16xx_Error_t fs16xx_pll_enable(Fs16xx_devId_t id, int enable) {
    Fs16xx_Error_t err;
    unsigned short value;

    /* Read PLLCTRL4 register, update OSCEN bit */
    err = fs16xx_read_register16(id, FS16XX_PLLCTRL4_REG, &value);
    if (Fs16xx_Error_OK == err) {
        if(enable != 0) {
            value |= FS16XX_PLLCTRL4_REG_PLLEN_MSK;
        } else {
            value &= (~FS16XX_PLLCTRL4_REG_PLLEN_MSK);
        }
        if(Fs16xx_Error_OK == err) {
            err = fs16xx_write_register16(id, FS16XX_PLLCTRL4_REG, value);
        }
    }
    return err;
}

/**
 * Fs16xx select pll reference.
 * @param id to opened instance
 * @param enable, 0 set disable, other values set enable
 */
Fs16xx_Error_t fs16xx_pll_ref_sel(Fs16xx_devId_t id, int sel) {
    Fs16xx_Error_t err;

    UNUSED(id);
    UNUSED(sel);
    return Fs16xx_Error_Not_Implemented;;
}

Fs16xx_Error_t fs16xx_wait_clk_stable(Fs16xx_devId_t id) {
    int max_try = 100;
    Fs16xx_Error_t err;
    unsigned short value;

    while(max_try > 0) {
        err = fs16xx_read_register16(id, FS16XX_STATUS_REG, &value);
        if(Fs16xx_Error_OK == err && ((value & FS16XX_STATUS_REG_PLLS) != 0)) {
            return Fs16xx_Error_OK;
        }
        max_try--;
        Sleep(1);
    }

    return Fs16xx_Error_StateTimedOut;
}

Fs16xx_Error_t fs16xx_wait_boost_ssend(Fs16xx_devId_t id) {
    int max_try = 10;
    Fs16xx_Error_t err;
    unsigned short value;

    while(max_try > 0) {
        err = fs16xx_read_register16(id, FS16XX_BSTCTRL_REG, &value);
        if(Fs16xx_Error_OK == err && ((value & FS16XX_BSTCTRL_REG_SSEND_MSK) != 0)) {
            return Fs16xx_Error_OK;
        }
        max_try--;
        Sleep(1);
    }

    return Fs16xx_Error_StateTimedOut;
}

