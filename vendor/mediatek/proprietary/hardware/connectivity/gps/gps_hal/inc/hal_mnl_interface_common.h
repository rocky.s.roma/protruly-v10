#ifndef __HAL_MNL_INTERFACE_COMMON_H__
#define __HAL_MNL_INTERFACE_COMMON_H__

#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <sys/socket.h>

#ifdef __cplusplus
extern "C" {
#endif

#define HAL_MNL_BUFF_SIZE           (16 * 1024)
#define HAL_MNL_INTERFACE_VERSION   1

//======================================================
// GPS HAL -> MNLD
//======================================================
#define MTK_HAL2MNL "mtk_hal2mnl"

typedef enum {
    HAL2MNL_HAL_REBOOT                      = 0,
    HAL2MNL_GPS_INIT                        = 101,
    HAL2MNL_GPS_START                       = 102,
    HAL2MNL_GPS_STOP                        = 103,
    HAL2MNL_GPS_CLEANUP                     = 104,
    HAL2MNL_GPS_INJECT_TIME                 = 105,
    HAL2MNL_GPS_INJECT_LOCATION             = 106,
    HAL2MNL_GPS_DELETE_AIDING_DATA          = 107,
    HAL2MNL_GPS_SET_POSITION_MODE           = 108,
    HAL2MNL_DATA_CONN_OPEN                  = 201,
    HAL2MNL_DATA_CONN_OPEN_WITH_APN_IP_TYPE = 202,
    HAL2MNL_DATA_CONN_CLOSED                = 203,
    HAL2MNL_DATA_CONN_FAILED                = 204,
    HAL2MNL_SET_SERVER                      = 301,
    HAL2MNL_SET_REF_LOCATION                = 302,
    HAL2MNL_SET_ID                          = 303,
    HAL2MNL_NI_MESSAGE                      = 401,
    HAL2MNL_NI_RESPOND                      = 402,
    HAL2MNL_UPDATE_NETWORK_STATE            = 501,
    HAL2MNL_UPDATE_NETWORK_AVAILABILITY     = 502,
    HAL2MNL_GPS_MEASUREMENT                 = 601,
    HAL2MNL_GPS_NAVIGATION                  = 602,
} hal2mnl_cmd;

typedef int gps_pos_mode;
#define GPS_POS_MODE_STANDALONE     0
#define GPS_POS_MODE_MSB            1

typedef int gps_pos_recurrence;
#define GPS_POS_RECURRENCE_PERIODIC     0
#define GPS_POS_RECURRENCE_SINGLE       1

typedef int ni_user_response_type;
#define NI_USER_RESPONSE_ACCEPT     1
#define NI_USER_RESPONSE_DENY       2
#define NI_USER_RESPONSE_NORESP     3

typedef int cell_type;
#define CELL_TYPE_GSM       1
#define CELL_TYPE_UMTS      2

typedef int agps_id_type;
#define AGPS_ID_TYPE_NONE       0
#define AGPS_ID_TYPE_IMSI       1
#define AGPS_ID_TYPE_MSISDN     2

typedef int network_type;
#define NETWORK_TYPE_MOBILE         0
#define NETWORK_TYPE_WIFI           1
#define NETWORK_TYPE_MOBILE_MMS     2
#define NETWORK_TYPE_MOBILE_SUPL    3
#define NETWORK_TYPE_MOBILE_DUN     4
#define NETWORK_TYPE_MOBILE_HIPRI   5
#define NETWORK_TYPE_WIMAX          6

typedef int apn_ip_type;
#define APN_IP_INVALID          0
#define APN_IP_IPV4             1
#define APN_IP_IPV6             2
#define APN_IP_IPV4V6           3

typedef int agps_type;
#define AGPS_TYPE_SUPL          1
#define AGPS_TYPE_C2K           2

//======================================================
// MNLD -> GPS HAL
//======================================================
#define MTK_MNL2HAL "mtk_mnl2hal"

typedef enum {
    MNL2HAL_MNLD_REBOOT             = 1,
    MNL2HAL_LOCATION                = 101,
    MNL2HAL_GPS_STATUS              = 102,
    MNL2HAL_GPS_SV                  = 103,
    MNL2HAL_NMEA                    = 104,
    MNL2HAL_GPS_CAPABILITIES        = 105,
    MNL2HAL_GPS_MEASUREMENTS        = 106,
    MNL2HAL_GPS_NAVIGATION          = 107,
    MNL2HAL_REQUEST_WAKELOCK        = 201,
    MNL2HAL_RELEASE_WAKELOCK        = 202,
    MNL2HAL_REQUEST_UTC_TIME        = 301,
    MNL2HAL_REQUEST_DATA_CONN       = 302,
    MNL2HAL_RELEASE_DATA_CONN       = 303,
    MNL2HAL_REQUEST_NI_NOTIFY       = 304,
    MNL2HAL_REQUEST_SET_ID          = 305,
    MNL2HAL_REQUEST_REF_LOC         = 306,
} mnl2hal_cmd;

#define GNSS_MAX_SVS            256
#define GPS_MAX_MEASUREMENT     32

typedef int gps_location_flags;
#define GPS_LOCATION_HAS_LAT_LONG   0x0001
#define GPS_LOCATION_HAS_ALT        0x0002
#define GPS_LOCATION_HAS_SPEED      0x0004
#define GPS_LOCATION_HAS_BEARING    0x0008
#define GPS_LOCATION_HAS_ACCURACY   0x0010

typedef int gps_capabilites;
#define GPS_CAP_SCHEDULING       0x0000001
#define GPS_CAP_MSB              0x0000002
#define GPS_CAP_MSA              0x0000004
#define GPS_CAP_SINGLE_SHOT      0x0000008
#define GPS_CAP_ON_DEMAND_TIME   0x0000010
#define GPS_CAP_GEOFENCING       0x0000020
#define GPS_CAP_MEASUREMENTS     0x0000040
#define GPS_CAP_NAV_MESSAGES     0x0000080

typedef int request_setid;
#define REQUEST_SETID_IMSI     (1<<0L)
#define REQUEST_SETID_MSISDN   (1<<1L)

typedef int request_refloc;
#define REQUEST_REFLOC_CELLID  (1<<0L)
#define REQUEST_REFLOC_MAC     (1<<1L)   // not ready

typedef short gps_clock_flags;
#define GPS_CLK_HAS_LEAP_SECOND               (1<<0)
#define GPS_CLK_HAS_TIME_UNCERTAINTY          (1<<1)
#define GPS_CLK_HAS_FULL_BIAS                 (1<<2)
#define GPS_CLK_HAS_BIAS                      (1<<3)
#define GPS_CLK_HAS_BIAS_UNCERTAINTY          (1<<4)
#define GPS_CLK_HAS_DRIFT                     (1<<5)
#define GPS_CLK_HAS_DRIFT_UNCERTAINTY         (1<<6)

typedef char gps_clock_type;
#define GPS_CLOCK_TYPE_UNKNOWN                  0
#define GPS_CLOCK_TYPE_LOCAL_HW_TIME            1
#define GPS_CLOCK_TYPE_GPS_TIME                 2

typedef int gps_measurement_flags;
#define GPS_MEASUREMENT_HAS_SNR                               (1<<0)
#define GPS_MEASUREMENT_HAS_ELEVATION                         (1<<1)
#define GPS_MEASUREMENT_HAS_ELEVATION_UNCERTAINTY             (1<<2)
#define GPS_MEASUREMENT_HAS_AZIMUTH                           (1<<3)
#define GPS_MEASUREMENT_HAS_AZIMUTH_UNCERTAINTY               (1<<4)
#define GPS_MEASUREMENT_HAS_PSEUDORANGE                       (1<<5)
#define GPS_MEASUREMENT_HAS_PSEUDORANGE_UNCERTAINTY           (1<<6)
#define GPS_MEASUREMENT_HAS_CODE_PHASE                        (1<<7)
#define GPS_MEASUREMENT_HAS_CODE_PHASE_UNCERTAINTY            (1<<8)
#define GPS_MEASUREMENT_HAS_CARRIER_FREQUENCY                 (1<<9)
#define GPS_MEASUREMENT_HAS_CARRIER_CYCLES                    (1<<10)
#define GPS_MEASUREMENT_HAS_CARRIER_PHASE                     (1<<11)
#define GPS_MEASUREMENT_HAS_CARRIER_PHASE_UNCERTAINTY         (1<<12)
#define GPS_MEASUREMENT_HAS_BIT_NUMBER                        (1<<13)
#define GPS_MEASUREMENT_HAS_TIME_FROM_LAST_BIT                (1<<14)
#define GPS_MEASUREMENT_HAS_DOPPLER_SHIFT                     (1<<15)
#define GPS_MEASUREMENT_HAS_DOPPLER_SHIFT_UNCERTAINTY         (1<<16)
#define GPS_MEASUREMENT_HAS_USED_IN_FIX                       (1<<17)

typedef short gps_measurement_state;
#define GPS_MEASUREMENT_STATE_UNKNOWN                   0
#define GPS_MEASUREMENT_STATE_CODE_LOCK             (1<<0)
#define GPS_MEASUREMENT_STATE_BIT_SYNC              (1<<1)
#define GPS_MEASUREMENT_STATE_SUBFRAME_SYNC         (1<<2)
#define GPS_MEASUREMENT_STATE_TOW_DECODED           (1<<3)
#define GPS_MEASUREMENT_STATE_MSEC_AMBIGUOUS        (1<<4)

typedef short gps_accumulated_delta_range_state;
#define GPS_ADR_STATE_UNKNOWN                       0
#define GPS_ADR_STATE_VALID                     (1<<0)
#define GPS_ADR_STATE_RESET                     (1<<1)
#define GPS_ADR_STATE_CYCLE_SLIP                (1<<2)

typedef char gps_loss_of_lock;
#define GPS_LOSS_OF_LOCK_UNKNOWN                            0
#define GPS_LOSS_OF_LOCK_OK                                 1
#define GPS_LOSS_OF_LOCK_CYCLE_SLIP                         2

typedef char gps_multipath_indicator;
#define GPS_MULTIPATH_INDICATOR_UNKNOWN                 0
#define GPS_MULTIPATH_INDICATOR_DETECTED                1
#define GPS_MULTIPATH_INDICATOR_NOT_USED                2

typedef char gps_nav_msg_type;
#define GPS_NAV_MSG_TYPE_UNKNOWN         0
#define GPS_NAV_MSG_TYPE_L1CA            1
#define GPS_NAV_MSG_TYPE_L2CNAV          2
#define GPS_NAV_MSG_TYPE_L5CNAV          3
#define GPS_NAV_MSG_TYPE_CNAV2           4

typedef short nav_msg_status;
#define NAV_MSG_STATUS_UNKONW              0
#define NAV_MSG_STATUS_PARITY_PASSED   (1<<0)
#define NAV_MSG_STATUS_PARITY_REBUILT  (1<<1)

typedef int gps_status;
#define GPS_STATUS_SESSION_BEGIN        1
#define GPS_STATUS_SESSION_END          2
#define GPS_STATUS_SESSION_ENGINE_ON    3
#define GPS_STATUS_SESSION_ENGINE_OFF   4

typedef int agps_notify_type;
#define AGPS_NOTIFY_TYPE_NONE                       0
#define AGPS_NOTIFY_TYPE_NOTIFY_ONLY                1
#define AGPS_NOTIFY_TYPE_NOTIFY_ALLOW_NO_ANSWER     2
#define AGPS_NOTIFY_TYPE_NOTIFY_DENY_NO_ANSWER      3
#define AGPS_NOTIFY_TYPE_PRIVACY                    4

typedef int ni_encoding_type;
#define NI_ENCODING_TYPE_NONE   0
#define NI_ENCODING_TYPE_GSM7   1
#define NI_ENCODING_TYPE_UTF8   2
#define NI_ENCODING_TYPE_UCS2   3

typedef struct {
    gps_location_flags flags;
    double lat;
    double lng;
    double alt;
    float speed;
    float bearing;
    float accuracy;
    int64_t timestamp;
} gps_location;

typedef struct {
    int     prn;
    float   snr;
    float   elevation;
    float   azimuth;
    bool has_ephemeris;
    bool has_almanac;
    bool used_in_fix;
} gnss_sv;

typedef struct {
    int  num_svs;
    gnss_sv  sv_list[GNSS_MAX_SVS];
} gnss_sv_info;

typedef struct {
    int prn;
    bool used_in_fix;
} NmeaGSACash;

typedef struct {
    /** A set of flags indicating the validity of the fields in this data structure. */
    gps_clock_flags flags;

    /**
     * Leap second data.
     * The sign of the value is defined by the following equation:
     *      utc_time_ns = time_ns + (full_bias_ns + bias_ns) - leap_second * 1,000,000,000
     *
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_LEAP_SECOND.
     */
    short leap_second;

    /**
     * Indicates the type of time reported by the 'time_ns' field.
     */
    gps_clock_type type;

    /**
     * The GPS receiver internal clock value. This can be either the local hardware clock value
     * (GPS_CLOCK_TYPE_LOCAL_HW_TIME), or the current GPS time derived inside GPS receiver
     * (GPS_CLOCK_TYPE_GPS_TIME). The field 'type' defines the time reported.
     *
     * For local hardware clock, this value is expected to be monotonically increasing during
     * the reporting session. The real GPS time can be derived by compensating the 'full bias'
     * (when it is available) from this value.
     *
     * For GPS time, this value is expected to be the best estimation of current GPS time that GPS
     * receiver can achieve. Set the 'time uncertainty' appropriately when GPS time is specified.
     *
     * Sub-nanosecond accuracy can be provided by means of the 'bias' field.
     * The value contains the 'time uncertainty' in it.
     */
    int64_t time_ns;

    /**
     * 1-Sigma uncertainty associated with the clock's time in nanoseconds.
     * The uncertainty is represented as an absolute (single sided) value.
     *
     * This value should be set if GPS_CLOCK_TYPE_GPS_TIME is set.
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_TIME_UNCERTAINTY.
     */
    double time_uncertainty_ns;

    /**
     * The difference between hardware clock ('time' field) inside GPS receiver and the true GPS
     * time since 0000Z, January 6, 1980, in nanoseconds.
     * This value is used if and only if GPS_CLOCK_TYPE_LOCAL_HW_TIME is set, and GPS receiver
     * has solved the clock for GPS time.
     * The caller is responsible for using the 'bias uncertainty' field for quality check.
     *
     * The sign of the value is defined by the following equation:
     *      true time (GPS time) = time_ns + (full_bias_ns + bias_ns)
     *
     * This value contains the 'bias uncertainty' in it.
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_FULL_BIAS.

     */
    int64_t full_bias_ns;

    /**
     * Sub-nanosecond bias.
     * The value contains the 'bias uncertainty' in it.
     *
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_BIAS.
     */
    double bias_ns;

    /**
     * 1-Sigma uncertainty associated with the clock's bias in nanoseconds.
     * The uncertainty is represented as an absolute (single sided) value.
     *
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_BIAS_UNCERTAINTY.
     */
    double bias_uncertainty_ns;

    /**
     * The clock's drift in nanoseconds (per second).
     * A positive value means that the frequency is higher than the nominal frequency.
     *
     * The value contains the 'drift uncertainty' in it.
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_DRIFT.
     *
     * If GpsMeasurement's 'flags' field contains GPS_MEASUREMENT_HAS_UNCORRECTED_PSEUDORANGE_RATE,
     * it is encouraged that this field is also provided.
     */
    double drift_nsps;

    /**
     * 1-Sigma uncertainty associated with the clock's drift in nanoseconds (per second).
     * The uncertainty is represented as an absolute (single sided) value.
     *
     * If the data is available 'flags' must contain GPS_CLOCK_HAS_DRIFT_UNCERTAINTY.
     */
    double drift_uncertainty_nsps;
} gps_clock;

typedef struct {
    /** A set of flags indicating the validity of the fields in this data structure. */
    gps_measurement_flags flags;

    /**
     * Pseudo-random number in the range of [1, 32]
     * This is a Mandatory value.
     */
    int8_t prn;

    /**
     * Time offset at which the measurement was taken in nanoseconds.
     * The reference receiver's time is specified by GpsData::clock::time_ns and should be
     * interpreted in the same way as indicated by GpsClock::type.
     *
     * The sign of time_offset_ns is given by the following equation:
     *      measurement time = GpsClock::time_ns + time_offset_ns
     *
     * It provides an individual time-stamp for the measurement, and allows sub-nanosecond accuracy.
     * This is a Mandatory value.
     */
    double time_offset_ns;

    /**
     * Per satellite sync state. It represents the current sync state for the associated satellite.
     * Based on the sync state, the 'received GPS tow' field should be interpreted accordingly.
     * This is a Mandatory value.
     */
    gps_measurement_state state;

    /**
     * Received GPS Time-of-Week at the measurement time, in nanoseconds.
     * The value is relative to the beginning of the current GPS week.
     *
     * Given the highest sync state that can be achieved, per each satellite, valid range for
     * this field can be:
     *     Searching       : [ 0       ]   : GPS_MEASUREMENT_STATE_UNKNOWN
     *     C/A code lock   : [ 0   1ms ]   : GPS_MEASUREMENT_STATE_CODE_LOCK is set
     *     Bit sync        : [ 0  20ms ]   : GPS_MEASUREMENT_STATE_BIT_SYNC is set
     *     Subframe sync   : [ 0    6s ]   : GPS_MEASUREMENT_STATE_SUBFRAME_SYNC is set
     *     TOW decoded     : [ 0 1week ]   : GPS_MEASUREMENT_STATE_TOW_DECODED is set
     *
     * However, if there is any ambiguity in integer millisecond,
     * GPS_MEASUREMENT_STATE_MSEC_AMBIGUOUS should be set accordingly, in the 'state' field.
     * This value must be populated if 'state' != GPS_MEASUREMENT_STATE_UNKNOWN.
     */
    int64_t received_gps_tow_ns;

    /**
     * 1-Sigma uncertainty of the Received GPS Time-of-Week in nanoseconds.
     * This value must be populated if 'state' != GPS_MEASUREMENT_STATE_UNKNOWN.
     */
    int64_t received_gps_tow_uncertainty_ns;

    /**
     * Carrier-to-noise density in dB-Hz, in the range [0, 63].
     * It contains the measured C/N0 value for the signal at the antenna input.
     * This is a Mandatory value.
     */
    double c_n0_dbhz;

    /**
     * Pseudorange rate at the timestamp in m/s.
     * The value also includes the effects of the receiver clock frequency and satellite clock
     * frequency errors.
     *
     * The value includes the 'pseudorange rate uncertainty' in it.
     * A positive value indicates that the pseudorange is getting larger.
     * This is a Mandatory value.
     */
    double pseudorange_rate_mps;

    /**
     * 1-Sigma uncertainty of the pseudurange rate in m/s.
     * The uncertainty is represented as an absolute (single sided) value.
     * This is a Mandatory value.
     */
    double pseudorange_rate_uncertainty_mps;

    /**
     * Accumulated delta range's state. It indicates whether ADR is reset or there is a cycle slip
     * (indicating loss of lock).
     * This is a Mandatory value.
     */
    gps_accumulated_delta_range_state accumulated_delta_range_state;

    /**
     * Accumulated delta range since the last channel reset in meters.
     * A positive value indicates that the SV is moving away from the receiver.
     *
     * The sign of the 'accumulated delta range' and its relation to the sign of 'carrier phase'
     * is given by the equation:
     *          accumulated delta range = -k * carrier phase    (where k is a constant)
     *
     * This value must be populated if 'accumulated delta range state' != GPS_ADR_STATE_UNKNOWN.
     * However, it is expected that the data is only accurate when:
     *      'accumulated delta range state' == GPS_ADR_STATE_VALID.
     */
    double accumulated_delta_range_m;

    /**
     * 1-Sigma uncertainty of the accumulated delta range in meters.
     * The data is available if 'accumulated delta range state' != GPS_ADR_STATE_UNKNOWN.
     */
    double accumulated_delta_range_uncertainty_m;

    /**
     * Best derived Pseudorange by the chip-set, in meters.
     * The value contains the 'pseudorange uncertainty' in it.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_PSEUDORANGE.
     */
    double pseudorange_m;

    /**
     * 1-Sigma uncertainty of the pseudorange in meters.
     * The value contains the 'pseudorange' and 'clock' uncertainty in it.
     * The uncertainty is represented as an absolute (single sided) value.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_PSEUDORANGE_UNCERTAINTY.
     */
    double pseudorange_uncertainty_m;

    /**
     * A fraction of the current C/A code cycle, in the range [0.0, 1023.0]
     * This value contains the time (in Chip units) since the last C/A code cycle (GPS Msec epoch).
     *
     * The reference frequency is given by the field 'carrier_frequency_hz'.
     * The value contains the 'code-phase uncertainty' in it.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_CODE_PHASE.
     */
    double code_phase_chips;

    /**
     * 1-Sigma uncertainty of the code-phase, in a fraction of chips.
     * The uncertainty is represented as an absolute (single sided) value.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_CODE_PHASE_UNCERTAINTY.
     */
    double code_phase_uncertainty_chips;

    /**
     * Carrier frequency at which codes and messages are modulated, it can be L1 or L2.
     * If the field is not set, the carrier frequency is assumed to be L1.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_CARRIER_FREQUENCY.
     */
    float carrier_frequency_hz;

    /**
     * The number of full carrier cycles between the satellite and the receiver.
     * The reference frequency is given by the field 'carrier_frequency_hz'.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_CARRIER_CYCLES.
     */
    int64_t carrier_cycles;

    /**
     * The RF phase detected by the receiver, in the range [0.0, 1.0].
     * This is usually the fractional part of the complete carrier phase measurement.
     *
     * The reference frequency is given by the field 'carrier_frequency_hz'.
     * The value contains the 'carrier-phase uncertainty' in it.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_CARRIER_PHASE.
     */
    double carrier_phase;

    /**
     * 1-Sigma uncertainty of the carrier-phase.
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_CARRIER_PHASE_UNCERTAINTY.
     */
    double carrier_phase_uncertainty;

    /**
     * An enumeration that indicates the 'loss of lock' state of the event.
     */
    gps_loss_of_lock loss_of_lock;

    /**
     * The number of GPS bits transmitted since Sat-Sun midnight (GPS week).
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_BIT_NUMBER.
     */
    int32_t bit_number;

    /**
     * The elapsed time since the last received bit in milliseconds, in the range [0, 20]
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_TIME_FROM_LAST_BIT.
     */
    int16_t time_from_last_bit_ms;

    /**
     * Doppler shift in Hz.
     * A positive value indicates that the SV is moving toward the receiver.
     *
     * The reference frequency is given by the field 'carrier_frequency_hz'.
     * The value contains the 'doppler shift uncertainty' in it.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_DOPPLER_SHIFT.
     */
    double doppler_shift_hz;

    /**
     * 1-Sigma uncertainty of the doppler shift in Hz.
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_DOPPLER_SHIFT_UNCERTAINTY.
     */
    double doppler_shift_uncertainty_hz;

    /**
     * An enumeration that indicates the 'multipath' state of the event.
     */
    gps_multipath_indicator multipath_indicator;

    /**
     * Signal-to-noise ratio in dB.
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_SNR.
     */
    double snr_db;

    /**
     * Elevation in degrees, the valid range is [-90, 90].
     * The value contains the 'elevation uncertainty' in it.
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_ELEVATION.
     */
    double elevation_deg;

    /**
     * 1-Sigma uncertainty of the elevation in degrees, the valid range is [0, 90].
     * The uncertainty is represented as the absolute (single sided) value.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_ELEVATION_UNCERTAINTY.
     */
    double elevation_uncertainty_deg;

    /**
     * Azimuth in degrees, in the range [0, 360).
     * The value contains the 'azimuth uncertainty' in it.
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_AZIMUTH.
     *  */
    double azimuth_deg;

    /**
     * 1-Sigma uncertainty of the azimuth in degrees, the valid range is [0, 180].
     * The uncertainty is represented as an absolute (single sided) value.
     *
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_AZIMUTH_UNCERTAINTY.
     */
    double azimuth_uncertainty_deg;

    /**
     * Whether the GPS represented by the measurement was used for computing the most recent fix.
     * If the data is available, 'flags' must contain GPS_MEASUREMENT_HAS_USED_IN_FIX.
     */
    bool used_in_fix;
} gps_measurement;

typedef struct {
    /** Number of measurements. */
    size_t measurement_count;

    /** The array of measurements. */
    gps_measurement measurements[GPS_MAX_MEASUREMENT];

    /** The GPS clock time reading. */
    gps_clock clock;
} gps_data;

typedef struct {
    /**
     * Pseudo-random number in the range of [1, 32]
     * This is a Mandatory value.
     */
    int8_t prn;

    /**
     * The type of message contained in the structure.
     * This is a Mandatory value.
     */
    gps_nav_msg_type type;

    /**
     * The status of the received navigation message.
     * No need to send any navigation message that contains words with parity error and cannot be
     * corrected.
     */
    nav_msg_status status;

    /**
     * Message identifier.
     * It provides an index so the complete Navigation Message can be assembled. i.e. fo L1 C/A
     * subframe 4 and 5, this value corresponds to the 'frame id' of the navigation message.
     * Subframe 1, 2, 3 does not contain a 'frame id' and this value can be set to -1.
     */
    short message_id;

    /**
     * Sub-message identifier.
     * If required by the message 'type', this value contains a sub-index within the current
     * message (or frame) that is being transmitted.
     * i.e. for L1 C/A the submessage id corresponds to the sub-frame id of the navigation message.
     */
    short submessage_id;

    /**
     * The length of the data (in bytes) contained in the current message.
     * If this value is different from zero, 'data' must point to an array of the same size.
     * e.g. for L1 C/A the size of the sub-frame will be 40 bytes (10 words, 30 bits/word).
     * This is a Mandatory value.
     */
    size_t data_length;

    /**
     * The data of the reported GPS message.
     * The bytes (or words) specified using big endian format (MSB first).
     *
     * For L1 C/A, each subframe contains 10 30-bit GPS words. Each GPS word (30 bits) should be
     * fitted into the last 30 bits in a 4-byte word (skip B31 and B32), with MSB first.
     */
    char data[40];
} gps_nav_msg;

void dump_gps_location(gps_location in);
void dump_gnss_sv(gnss_sv in);
void dump_gnss_sv_info(gnss_sv_info in);
void dump_gps_data(gps_data in);
void dump_gps_measurement(gps_measurement in);
void dump_gps_clock(gps_clock in);
void dump_gps_nav_msg(gps_nav_msg in);

#ifdef __cplusplus
}
#endif

#endif

