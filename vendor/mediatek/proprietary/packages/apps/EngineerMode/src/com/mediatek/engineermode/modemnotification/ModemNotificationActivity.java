/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.modemnotification;

import android.app.AlertDialog;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.Toast;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.eminfo.Content;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.R;


import java.util.ArrayList;
import java.util.List;


/**
 * MD Notification
 */
public class ModemNotificationActivity extends Activity {
    private static final String TAG = "EM/ModemNotification";
    private static final String MODEM_NOTIFICATION_SHAREPRE =
           "telephony_modem_notification_settings";
    private static final int TOTAL_NOTIFICATION_ITEM = 2;
    private RadioButton mCADetectedOn;
    private RadioButton mCADetectedOff;
    private RadioButton mCAToCAOn;
    private RadioButton mCAToCAOff;
    private int mCheckedShare = 0;
    private int mFlag = 0;
    private int[] mChecked;
    private Phone mPhone = null;
    private int[] EM_TYPES = new int[] {Content.ERRC_EM_CONN_INFO,
                Content.ERRC_EM_MOB_EVENT_IND};

    private static final int MSG_NW_INFO = 1;
    private static final int MSG_NW_INFO_OPEN = 3;
    private static final int MSG_NW_INFO_CLOSE = 4;
    private static final int FLAG_OR_DATA = 0xFFFFFFF7;
    private static final int FLAG_OFFSET_BIT = 0x08;
    private static final int FLAG_DATA_BIT = 8;
    private Context mContext;


    private static boolean isServiceRunning(Context context, String className) {
         boolean isRunning = false;
         ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
         int maxCount = 100;
         List<ActivityManager.RunningServiceInfo> runningServices =
             am.getRunningServices(maxCount);
         while (runningServices.size() == maxCount) {
             maxCount += 50;
             runningServices = am.getRunningServices(maxCount);
         }

         for (int i = 0; i < runningServices.size(); i++) {
             ActivityManager.RunningServiceInfo info = runningServices.get(i);
             //Elog.d(TAG, "isServiceRunning service name " + info.service.getClassName());
             if (info.service.getClassName().equals(className)) {
                 isRunning = true;
                 break;
             }
         }

         Elog.d(TAG, "isServiceRunning isRunning " + isRunning);
         return isRunning;
     }

    private Handler mATCmdHander = new Handler() {
        public void handleMessage(Message msg) {
                AsyncResult ar;
                switch (msg.what) {
                case MSG_NW_INFO:
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception == null) {
                        String[] data = (String[]) ar.result;
                            if ((data.length > 0) && (data[0] != null)) {
                            Elog.v(TAG, "data[0] is : " + data[0]);
                            Elog.v(TAG, "flag is : " + data[0].substring(FLAG_DATA_BIT));
                            mFlag = Integer.valueOf(data[0].substring(FLAG_DATA_BIT));
                            mFlag = mFlag | FLAG_OFFSET_BIT;
                            Elog.v(TAG, "mFlag change is : " + mFlag);
                            //for (Integer j : mCheckedEmTypes) {
                            //    String[] atCommand = new String[2];
                            //    atCommand[0] = "AT+EINFO=" + mFlag + "," + j + ",0";
                            //    atCommand[1] = "+EINFO";
                            //    sendATCommand(atCommand, MSG_NW_INFO_OPEN);
                            //}
                        }
                    }
                    break;
                case MSG_NW_INFO_OPEN:
                case MSG_NW_INFO_CLOSE:
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception != null) {
                        Elog.e(TAG, "AT cmd confirm error");
                    }
                   break;
                default:
                    break;
                }
            }
        };

    private CompoundButton.OnCheckedChangeListener mRadioListener =
        new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                if (buttonView == mCADetectedOn) {
                    mChecked[0] = 1;
                    writeCheckedSharedPreference();
                    if (!isServiceRunning(mContext,
                        "com.mediatek.engineermode.modemnotification.ModemNotificationService")) {
                        Intent intent = new Intent(mContext, ModemNotificationService.class);
                        startService(intent);
                        writeSharedPreference(true);
                    }
                    StartNotification(EM_TYPES[0]);

                } else if (buttonView == mCADetectedOff) {
                    mChecked[0] = 0;
                    writeCheckedSharedPreference();
                    StopNotification(EM_TYPES[0]);
                } else if (buttonView == mCAToCAOn) {
                    mChecked[1] = 1;
                    writeCheckedSharedPreference();
                    if (!isServiceRunning(mContext,
                        "com.mediatek.engineermode.modemnotification.ModemNotificationService")) {
                        Intent intent = new Intent(mContext, ModemNotificationService.class);
                        startService(intent);
                        writeSharedPreference(true);
                    }
                    StartNotification(EM_TYPES[1]);
                } else if (buttonView == mCAToCAOff) {
                    mChecked[1] = 0;
                    writeCheckedSharedPreference();
                    StopNotification(EM_TYPES[1]);
                }
            }
         }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.md_em_notification);

        mContext = this;

        if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            mPhone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
        } else {
            mPhone = PhoneFactory.getDefaultPhone();
        }
        final SharedPreferences modemNotificationSh = getSharedPreferences(
            MODEM_NOTIFICATION_SHAREPRE, android.content.Context.MODE_PRIVATE);
        int checkedOn = modemNotificationSh.getInt(getString(R.string.md_checkedshare_value), 0);

        Elog.d(TAG, "onCreate checkedOn " + checkedOn);

        mChecked = new int[TOTAL_NOTIFICATION_ITEM];

        for (int i = 0; i < TOTAL_NOTIFICATION_ITEM; i++) {
            mChecked[i] = (checkedOn >> i) & 0x0001;
            Elog.d(TAG, "onCreate i: " + i + " mChecked[i]: " + mChecked[i]);
        }

        mCADetectedOn = (RadioButton) findViewById(R.id.radio_ca_detected_on);
        mCADetectedOff = (RadioButton) findViewById(R.id.radio_ca_detected_off);
        mCAToCAOn = (RadioButton) findViewById(R.id.radio_ca_to_ca_on);
        mCAToCAOff = (RadioButton) findViewById(R.id.radio_ca_to_ca_off);

        if (mChecked[0] == 1) {
            mCADetectedOn.setChecked(true);
        } else {
            mCADetectedOff.setChecked(true);
        }
        if (mChecked[1] == 1) {
            mCAToCAOn.setChecked(true);
        } else {
            mCAToCAOff.setChecked(true);
        }

        mCADetectedOn.setOnCheckedChangeListener(mRadioListener);
        mCADetectedOff.setOnCheckedChangeListener(mRadioListener);
        mCAToCAOn.setOnCheckedChangeListener(mRadioListener);
        mCAToCAOff.setOnCheckedChangeListener(mRadioListener);

        registerNetwork();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        boolean isAnyChecked = false;
        for (int i = 0; i < TOTAL_NOTIFICATION_ITEM; i++) {
            Elog.d(TAG, "onClick i: " + i + " mChecked[i]: " + mChecked[i]);
            if (mChecked[i] == 1) {
                isAnyChecked = true;
            }
                    }
            if (!isAnyChecked) {
            //stop service
                if (isServiceRunning(mContext,
                        "com.mediatek.engineermode.modemnotification.ModemNotificationService")) {
                    stopService(new Intent(mContext, ModemNotificationService.class));
                    writeSharedPreference(false);
                    unregisterNetwork();
                }
            }
        super.onDestroy();
    }

    private void writeCheckedSharedPreference() {
        final SharedPreferences modemNotificationSh = getSharedPreferences(
            MODEM_NOTIFICATION_SHAREPRE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = modemNotificationSh.edit();
        int checked = 0;
        for (int i = 0; i < TOTAL_NOTIFICATION_ITEM; i++) {
            checked = checked | (mChecked[i] << i);
        }
        Elog.d(TAG, "writeCheckedSharedPreference checked " + checked);
        editor.putInt(getString(R.string.md_checkedshare_value), checked);
        editor.commit();
    }

    private void writeSharedPreference(boolean flag) {
        final SharedPreferences modemNotificationSh = getSharedPreferences(
            MODEM_NOTIFICATION_SHAREPRE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = modemNotificationSh.edit();
        editor.putBoolean(getString(R.string.md_notification_value), flag);
        editor.commit();
    }

    private void registerNetwork() {
        Elog.d(TAG, "registerNetwork");
        String[] atCommand = {"AT+EINFO?", "+EINFO"};
        sendATCommand(atCommand, MSG_NW_INFO);
    }

    private void unregisterNetwork() {
        Elog.d(TAG, "unregisterNetwork");

        mFlag = mFlag & FLAG_OR_DATA;
        Elog.v(TAG, "The close flag is :" + mFlag);
        String[] atCloseCmd = new String[2];
        atCloseCmd[0] = "AT+EINFO=" + mFlag;
        atCloseCmd[1] = "";
        sendATCommand(atCloseCmd, MSG_NW_INFO_CLOSE);
    }

    private void sendATCommand(String[] atCommand, Message msg) {
        if (mPhone != null) {
            Elog.d(TAG, "sendATCommand " + atCommand[0]);
            mPhone.invokeOemRilRequestStrings(atCommand, msg);
        }
    }

    private void sendATCommand(String[] atCommand, int msg) {
        if (mPhone != null) {
            Elog.d(TAG, "sendATCommand " + atCommand[0]);
            mPhone.invokeOemRilRequestStrings(atCommand, mATCmdHander.obtainMessage(msg));
        }
    }

    private void StartNotification(int type) {
        String[] atCommand = new String[2];
        atCommand[0] = "AT+EINFO=" + mFlag + "," + type + ",0";
        atCommand[1] = "+EINFO";
        sendATCommand(atCommand, MSG_NW_INFO_OPEN);
    }

    private void StopNotification(int type) {
        String[] atCommand = new String[2];
        atCommand[0] = "AT+EINFO=" + mFlag + "," + type + ",1";
        atCommand[1] = "+EINFO";
        sendATCommand(atCommand, MSG_NW_INFO_CLOSE);
    }

}
