package com.mediatek.deviceregister;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.mediatek.deviceregister.utils.AgentProxy;
import com.mediatek.deviceregister.utils.PlatformManager;

public class RegisterReceiver extends BroadcastReceiver {

    private static final String TAG = Const.TAG_PREFIX + "RegisterReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "onReceive " + intent);

        if (!isSwitchOpen()) {
            Log.i(TAG, "Feature is not enabled, do nothing");
            android.os.Process.killProcess(android.os.Process.myPid());
            return;
        }

        String action = intent.getAction();
        if (action.equalsIgnoreCase(Const.ACTION_BOOTCOMPLETED)) {
            PlatformManager.clearPreferences(context);
        }   else if (action.equalsIgnoreCase(Const.ACTION_PRE_BOOT_COMPLETED) ||
                     action.equalsIgnoreCase(Const.ACTION_CT_CONFIRMED_MESSAGE)) {
            goToService(context, intent);
        } else if (Const.ACTION_SMS_STATE_CHANGED.equalsIgnoreCase(action)) {
            if (intent.getBooleanExtra("ready", false)) {
                smsOrsimReady(context, intent);
            }

        } else if (Const.ACTION_CDMA_CARD_ESN_OR_MEID.equalsIgnoreCase(action)) {
            String meidOrEsn = intent.getStringExtra("esn_or_meid");
            Log.i(TAG, "Card esn or meid is : " + meidOrEsn);
            SharedPreferences sharedPrf = PlatformManager.getUniquePreferences(context);
            boolean notFirst = sharedPrf.getBoolean(Const.PRE_KEY_NOT_FIRST_SUBINFO, false);
            boolean receivedCardMeidOrEsn =
                    AgentProxy.getInstance().getReceivedMeidFlag();
            if (notFirst && receivedCardMeidOrEsn) {
                Log.i(TAG, "Ignore duplicate messages, do nothing.");
                return;
            }
            if (!receivedCardMeidOrEsn) {
                AgentProxy.getInstance().setReceivedMeidFlag(true);
                AgentProxy.getInstance().setReceivedMeid(meidOrEsn);
            }
            if (notFirst) {
                goToService(context, intent);
            }
        } else if (Const.ACTION_SHUTDOWN_IPO.equalsIgnoreCase(action)) {
            AgentProxy.getInstance().setReceivedMeidFlag(false);
        } else if (action.equalsIgnoreCase(Const.ACTION_SIM_STATE_CHANGED)) {

            if (intent.getStringExtra(Const.KEY_ICC_STATE).equals(Const.VALUE_ICC_LOADED)) {
                smsOrsimReady(context, intent);
            }
        }
    }

    private void smsOrsimReady(Context context, Intent intent) {
        SharedPreferences sharedPrf = PlatformManager.getUniquePreferences(context);
        boolean notFirst = sharedPrf.getBoolean(Const.PRE_KEY_NOT_FIRST_SUBINFO, false);
        boolean receivedCardMeidOrEsn =
                AgentProxy.getInstance().getReceivedMeidFlag();
        Log.i(TAG, "notFirst / receivedCardMeidOrEsn : "
                + notFirst +"/" + receivedCardMeidOrEsn);
        if (notFirst && receivedCardMeidOrEsn) {
            Log.i(TAG, "Ignore duplicate messages, do nothing.");
            return;
        }
        if (!notFirst) {
            sharedPrf.edit().putBoolean(Const.PRE_KEY_NOT_FIRST_SUBINFO, true).commit();
        }
        if (receivedCardMeidOrEsn) {
            goToService(context, intent);
        } else {
            Log.i(TAG, "Not received ESN/MEID msg, wait.");
        }
    }

    private void goToService(Context context, Intent intent) {
        intent.setClass(context, RegisterService.class);
        context.startService(intent);
    }

    private boolean isSwitchOpen() {
        return AgentProxy.getInstance().isFeatureEnabled();
    }

}
