// Flash AWB tuning parameter
{
    9, //foreground percentage
    95, //background percentage
    2, //FgPercentage_Th1
    5, //FgPercentage_Th2
    10, //FgPercentage_Th3
    15, //FgPercentage_Th4
    250, //FgPercentage_Th1_Val
    250, //FgPercentage_Th2_Val
    250, //FgPercentage_Th3_Val
    250, //FgPercentage_Th4_Val
    10, //location_map_th1
    20, //location_map_th2
    40, //location_map_th3
    50, //location_map_th4
    100, //location_map_val1
    200, //location_map_val2
    300, //location_map_val3
    400, //location_map_val4
    0, //SelfTuningFbBgWeightTbl
    100, //FgBgTbl_Y0
    100, //FgBgTbl_Y1
    100, //FgBgTbl_Y2
    100, //FgBgTbl_Y3
    100, //FgBgTbl_Y4
    100, //FgBgTbl_Y5
    5, //YPrimeWeightTh[0]
    9, //YPrimeWeightTh[1]
    11, //YPrimeWeightTh[2]
    13, //YPrimeWeightTh[3]
    15, //YPrimeWeightTh[4]
    1, //YPrimeWeight[0]
    3, //YPrimeWeight[1]
    5, //YPrimeWeight[2]
    7, //YPrimeWeight[3]
    512, //FlashPreferenceGain R
    512, //FlashPreferenceGain G
    512, //FlashPreferenceGain B
},

// Flash AWB calibration
{{

    {867, 512, 759},
    {868, 512, 757},
    {869, 512, 754},
    {869, 512, 751},
    {870, 512, 747},
    {871, 512, 744},
    {871, 512, 739},
    {872, 512, 736},
    {873, 512, 733},
    {873, 512, 728},
    {874, 512, 724},
    {875, 512, 720},
    {876, 512, 717},
    {877, 512, 713},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512},
    {512, 512, 512}
}}

