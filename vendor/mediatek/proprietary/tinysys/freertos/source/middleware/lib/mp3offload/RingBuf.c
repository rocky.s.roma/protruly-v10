/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2015. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*/
#include <FreeRTOS.h>
#include "RingBuf.h"
#include <string.h>
#include "audio_task_offload_mp3_params.h"

static void my_memcpy(const char *target, const char *source, size_t count)
{

    if (count == 0 || target == source) { /* nothing to do */
        return;
    }
    memcpy((void *)target, (void *)source, count);
}
static void liang_memcpy(const char *target, const char *source, size_t count,
                         uint8_t Dramplaystate)
{

    if (count == 0 || target == source) { /* nothing to do */
        return;
    }
    if (Dramplaystate == 0) { //sram
        memcpy((void *)target, (void *)source, count);
    } else { //dram
        mp3_dma_transaction_wrap((uint32_t)target, (uint32_t)source, count);
    }
}

/**
* function for get how many data is available
* @return how many data exist
*/
uint32_t RingBuf_getDataCount(const RingBuf *RingBuf1)
{
    int count = RingBuf1->pWrite - RingBuf1->pRead;
    if (count < 0) {
        count += RingBuf1->bufLen;
    }
    return count;
}


/**
*  function for get how free space available
* @return how free sapce
*/

uint32_t RingBuf_getFreeSpace(const RingBuf *RingBuf1)
{
    int count = 0;
    if (RingBuf1->pRead > RingBuf1->pWrite) {
        count = RingBuf1->pRead - RingBuf1->pWrite - 8;
    } else { // RingBuf1->pRead <= RingBuf1->pWrite
        count = RingBuf1->pRead - RingBuf1->pWrite + RingBuf1->bufLen - 8;
    }

    return (count > 0) ? count : 0;
}

/**
* copy count number bytes from ring buffer to buf
* @param buf buffer copy from
* @param RingBuf1 buffer copy to
* @param count number of bytes need to copy
*/
void RingBuf_copyToLinear(char *buf, RingBuf *RingBuf1, uint32_t count)
{
    if (count == 0) {
        return;
    }

    // if not enough, assert
    // ASSERT(RingBuf_getDataCount(RingBuf1) >= count);

    if (RingBuf1->pRead <= RingBuf1->pWrite) {
        my_memcpy(buf, RingBuf1->pRead, count);
        RingBuf1->pRead += count;
    } else {
        uint32_t r2e = RingBuf1->pBufEnd - RingBuf1->pRead;
        if (count <= r2e) {
            my_memcpy(buf, RingBuf1->pRead, count);
            RingBuf1->pRead += count;
            if (RingBuf1->pRead == RingBuf1->pBufEnd) {
                RingBuf1->pRead = RingBuf1->pBufBase;
            }
        } else {
            my_memcpy(buf, RingBuf1->pRead, r2e);
            my_memcpy(buf + r2e, RingBuf1->pBufBase, count - r2e);
            RingBuf1->pRead = RingBuf1->pBufBase + count - r2e;
        }
    }
}

/**
* copy count number bytes from buf to RingBuf1
* @param RingBuf1 ring buffer copy from
* @param buf copy to
* @param count number of bytes need to copy
*/

void RingBuf_copyFromLinear(RingBuf *RingBuf1, const char *buf, uint32_t count)
{
    //int spaceIHave;
    char *end = RingBuf1->pBufBase + RingBuf1->bufLen;
    // count buffer data I have
    // spaceIHave = RingBuf1->bufLen - RingBuf_getDataCount(RingBuf1) - 8;
    //spaceIHave = RingBuf_getDataCount(RingBuf1);

    // if not enough, assert
    //ASSERT(spaceIHave >= count);

    if (RingBuf1->pRead <= RingBuf1->pWrite) {
        int w2e = end - RingBuf1->pWrite;
        if (count <= w2e) {
            memcpy(RingBuf1->pWrite, buf, count);
            RingBuf1->pWrite += count;
            if (RingBuf1->pWrite == end) {
                RingBuf1->pWrite = RingBuf1->pBufBase;
            }
        } else {
            memcpy(RingBuf1->pWrite, buf, w2e);
            memcpy(RingBuf1->pBufBase, buf + w2e, count - w2e);
            RingBuf1->pWrite = RingBuf1->pBufBase + count - w2e;
        }
    } else {
        memcpy(RingBuf1->pWrite, buf, count);
        RingBuf1->pWrite += count;
    }

}

/**
* copy count number bytes from buf to RingBuf1
* @param RingBuf1 ring buffer copy from
* @param buf copy to
* @param count number of bytes need to copy
*/

void RingBuf_copyFromLinear_dma(RingBuf *RingBuf1, const char *buf,
                                uint32_t count, const char bDramPlayback)
{
    int spaceIHave;
    char *end = RingBuf1->pBufBase + RingBuf1->bufLen;
    // count buffer data I have
    spaceIHave = RingBuf1->bufLen - RingBuf_getDataCount(RingBuf1) - 8;
    spaceIHave = RingBuf_getDataCount(RingBuf1);

    // if not enough, assert
    if(spaceIHave < count)
        PRINTF_E("spaceIHave %d < count %d\n",spaceIHave,count);

    if (RingBuf1->pRead <= RingBuf1->pWrite) {
        int w2e = end - RingBuf1->pWrite;
        if (count <= w2e) {
            liang_memcpy(RingBuf1->pWrite, buf, count, bDramPlayback);
            RingBuf1->pWrite += count;
            if (RingBuf1->pWrite == end) {
                RingBuf1->pWrite = RingBuf1->pBufBase;
            }
        } else {
            liang_memcpy(RingBuf1->pWrite, buf, w2e, bDramPlayback);
            liang_memcpy(RingBuf1->pBufBase, buf + w2e, count - w2e, bDramPlayback);
            RingBuf1->pWrite = RingBuf1->pBufBase + count - w2e;
        }
    } else {
        liang_memcpy(RingBuf1->pWrite, buf, count, bDramPlayback);
        RingBuf1->pWrite += count;
    }

}

/**
* copy ring buffer from RingBufs(source) to RingBuft(target)
* @param RingBuft ring buffer copy to
* @param RingBufs copy from copy from
*/
void RingBuf_copyFromRingBufAll(RingBuf *RingBuft, RingBuf *RingBufs)
{
    // if not enough, assert
    //   ASSERT(RingBuf_getFreeSpace(RingBuft) >= RingBuf_getDataCount(RingBufs));

    if (RingBufs->pRead <= RingBufs->pWrite) {
        RingBuf_copyFromLinear(RingBuft, RingBufs->pRead,
                               RingBufs->pWrite - RingBufs->pRead);
    } else {
        RingBuf_copyFromLinear(RingBuft, RingBufs->pRead,
                               RingBufs->pBufEnd - RingBufs->pRead);
        RingBuf_copyFromLinear(RingBuft, RingBufs->pBufBase,
                               RingBufs->pWrite  - RingBufs->pBufBase);
    }
    RingBufs->pRead = RingBufs->pWrite;
}


/**
* copy ring buffer from RingBufs(source) to RingBuft(target) with count
* @param RingBuft ring buffer copy to
* @param RingBufs copy from copy from
*/
int RingBuf_copyFromRingBuf(RingBuf *RingBuft, RingBuf *RingBufs, uint32_t count)
{
    if (count == 0) {
        return 0;
    }

    // if not enough, assert
    //  ASSERT(RingBuf_getDataCount(RingBufs) >= count &&
    //        RingBuf_getFreeSpace(RingBuft) >= count);

    if (RingBufs->pRead <= RingBufs->pWrite) {
        RingBuf_copyFromLinear(RingBuft, RingBufs->pRead, count);
        RingBufs->pRead += count;
    } else {
        uint32_t r2e = RingBufs->pBufEnd - RingBufs->pRead;
        if (r2e >= count) {
            RingBuf_copyFromLinear(RingBuft, RingBufs->pRead, count);
            RingBufs->pRead += count;
            if (RingBufs->pRead == RingBufs->pBufEnd) {
                RingBufs->pRead = RingBufs->pBufBase;
            }
        } else {
            RingBuf_copyFromLinear(RingBuft, RingBufs->pRead, r2e);
            RingBuf_copyFromLinear(RingBuft, RingBufs->pBufBase, count - r2e);
            RingBufs->pRead = RingBufs->pBufBase + count - r2e;
        }
    }
    return count;
}


/**
* write bytes size of count with value
* @param RingBuf1 ring buffer copy to
* @value value put into buffer
* @count bytes ned to put.
*/
void RingBuf_writeDataValue(RingBuf *RingBuf1, const char value,
                            const uint32_t count)
{
    if (count == 0) {
        return;
    }

    // if not enough, assert
    //   ASSERT(RingBuf_getFreeSpace(RingBuf1) >= count);

    if (RingBuf1->pRead <= RingBuf1->pWrite) {
        uint32_t w2e = RingBuf1->pBufEnd - RingBuf1->pWrite;
        if (count <= w2e) {
            memset(RingBuf1->pWrite, value, count);
            RingBuf1->pWrite += count;
            if (RingBuf1->pWrite == RingBuf1->pBufEnd) {
                RingBuf1->pWrite = RingBuf1->pBufBase;
            }
        } else {
            memset(RingBuf1->pWrite, value, w2e);
            memset(RingBuf1->pBufBase, value, count - w2e);
            RingBuf1->pWrite = RingBuf1->pBufBase + count - w2e;
        }
    } else {
        memset(RingBuf1->pWrite, value, count);
        RingBuf1->pWrite += count;
    }
}
