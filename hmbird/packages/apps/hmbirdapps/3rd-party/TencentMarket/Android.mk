LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := TencentMarket
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libaurora.so \
        lib/armeabi/libnativeexceptionhandler.so \
        lib/armeabi/libNativeRQD.so \
        lib/armeabi/libps.so \
        lib/armeabi/libqqndkfile_ex.so \
        lib/armeabi/libqqndkfile.so \
        lib/armeabi/libsecuritysdk_base.so \
        lib/armeabi/libsm_mq.so \
        lib/armeabi/libwatch.so \
        lib/armeabi/libyyb_csech.so \
        lib/armeabi/libyybpatch.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

