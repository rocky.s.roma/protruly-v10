LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Sina_Weibo
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libaccount.so  \
        lib/armeabi/libautooptfilter-armeabi-v7a.so  \
        lib/armeabi/libbarcode-armeabi-v7a.so  \
        lib/armeabi/libblink.so  \
        lib/armeabi/libbreakpad_intergration.so  \
        lib/armeabi/libcronet.so  \
        lib/armeabi/libdidi_secure.so  \
        lib/armeabi/libencoder.so  \
        lib/armeabi/libflybird.so  \
        lib/armeabi/libgenius_blur.so  \
        lib/armeabi/libgifdecoder.so  \
        lib/armeabi/libgifencoder-armeabi-v7a.so  \
        lib/armeabi/libgifencoder.so  \
        lib/armeabi/libgpufilterengine.so  \
        lib/armeabi/libgpuimage-library.so  \
        lib/armeabi/libimgreduce.so  \
        lib/armeabi/liblivenessdetection_v2.4.3.so  \
        lib/armeabi/libluckymoneyjni.so  \
        lib/armeabi/libmediapro.so  \
        lib/armeabi/libRCRTMakeupEngine.so  \
        lib/armeabi/libsgmain.so  \
        lib/armeabi/libsgsecuritybody.so  \
        lib/armeabi/libtbMPlayer.so  \
        lib/armeabi/libut_c_api.so  \
        lib/armeabi/libutility.so  \
        lib/armeabi/libuxijkplayer.so  \
        lib/armeabi/libuxijksdl.so  \
        lib/armeabi/libVideoEdit.so  \
        lib/armeabi/libweibocache.so  \
        lib/armeabi/libweiboffmpeg.so  \
        lib/armeabi/libweiboplayer.so  \
        lib/armeabi/libweibosdkcore.so  \
        lib/armeabi/libweibosdl.so  \
        lib/armeabi/libxiaoka.so  \
        lib/armeabi/libyuv.so  \
        lib/armeabi/libYXLiveMediaClientPlayer.so  \
        lib/armeabi/libYXLiveMediaClientPlayervideolive.so  \
        lib/armeabi/libYXLiveMediaClientPublisher.so  \
        lib/armeabi/libYXLiveMediaClientPublisherVideolive.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

