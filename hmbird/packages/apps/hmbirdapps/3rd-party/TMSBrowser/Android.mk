LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := TMSBrowser
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_PRIVILEGED_MODULE := true

LOCAL_OVERRIDES_PACKAGES := MtkBrowser Browser

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libAndro7za.so \
        lib/armeabi/libbase_module_dex.so \
        lib/armeabi/libbitmaps.so \
        lib/armeabi/libblur_armv7.so \
        lib/armeabi/libcmdsh.so \
        lib/armeabi/libcommon_basemodule_jni.so \
        lib/armeabi/libFdToFilePath.so \
        lib/armeabi/libFileNDK.so \
        lib/armeabi/libgif-jni.so \
        lib/armeabi/libmemchunk.so \
        lib/armeabi/libmttwebview_plat_support.so \
        lib/armeabi/libmttwebview.so \
        lib/armeabi/libNativeRQD.so \
        lib/armeabi/libresapk.so \
        lib/armeabi/libtbsconf.so \
        lib/armeabi/libtbs_sdk_extension_dex.so \
        lib/armeabi/libtbs_shell_dex.so \
        lib/armeabi/libTencentLocationSDK.so \
        lib/armeabi/libtencentpos.so \
        lib/armeabi/libTmsdk-2.0.8-mfr.so \
        lib/armeabi/libvideo_impl_dex.so \
        lib/armeabi/libwebp_base.so \
        lib/armeabi/libwebview_dex.so \
        lib/armeabi/libwtecdh.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

