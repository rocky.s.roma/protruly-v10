LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := LeYao
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
    lib/armeabi-v7a/libBugly.so \
    lib/armeabi-v7a/libglFilterCore.so \
    lib/armeabi-v7a/libgnustl_shared.so \
    lib/armeabi-v7a/libijkffmpeg.so \
    lib/armeabi-v7a/libijkplayer.so \
    lib/armeabi-v7a/libijksdl.so \
    lib/armeabi-v7a/liblocSDK7.so \
    lib/armeabi-v7a/libpl_droidsonroids_gif.so \
    lib/armeabi-v7a/librealm-jni.so \
    lib/armeabi-v7a/libRongIMLib.so \
    lib/armeabi-v7a/libRtmpPushCore.so \
    lib/armeabi-v7a/libucrop.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

