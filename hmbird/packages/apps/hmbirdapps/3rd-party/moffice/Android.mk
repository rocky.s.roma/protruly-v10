LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := moffice
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MULTILIB := 32

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libBDRC4_V1_1.so \
        lib/armeabi/libdoc_encrypt.so \
        lib/armeabi/libfreetype-jni.so \
        lib/armeabi/libgetuiext2.so \
        lib/armeabi/libKSOStatEncryption.so \
        lib/armeabi/libkwopdf.so \
        lib/armeabi/libOoxmlDecrypt.so \
        lib/armeabi/libweibosdkcore.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

