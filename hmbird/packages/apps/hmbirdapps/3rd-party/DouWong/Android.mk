LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := DouWong
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/armeabi/libandroid-swfplayer.so \
        lib/armeabi/libAVCDecoder.so \
        lib/armeabi/libgensee-log.so \
        lib/armeabi/libgsolcomp-jni.so \
        lib/armeabi/libH264Android.so \
        lib/armeabi/libSoundTouch.so \
        lib/armeabi/libspeex.so \
        lib/armeabi/libstlport_shared.so \
        lib/armeabi/libSwfView.so \
        lib/armeabi/libucamf.so \
        lib/armeabi/libucbase.so \
        lib/armeabi/libucdflvreader.so \
        lib/armeabi/libucflv.so \
        lib/armeabi/libucjpeg.so \
        lib/armeabi/libucnet.so \
        lib/armeabi/libucoffplayer.so \
        lib/armeabi/libucpdu.so \
        lib/armeabi/libucpingpdu.so \
        lib/armeabi/libucrtmpcli.so \
        lib/armeabi/libucrtp.so \
        lib/armeabi/libuctinyxml.so \
        lib/armeabi/libucts.so \
        lib/armeabi/libucwcc.so \
        lib/armeabi/libunidecoder.so

LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

